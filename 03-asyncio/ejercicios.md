# Protocolos para la transmisión de audio y video en Internet
# Envío asincrónico de datos UDP. Ejercicio de entrega (minipráctica).

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.66 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de uso de wireshark
* Nociones de UDP (de cursos anteriores)
* Nociones de SDP (de temas anteriores)

**Tiempo estimado:** 8 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2024-2025/03-asyncio-entrega

**Fecha de entrega parte individual:** 31 de octubre de 2024, 23:59 (hasta ejercicio 8, incluido)

**Fecha de entrega parte interoperación:** 6 de noviembre de 2024, 23:59 (ejercicio 9)

## Introducción

El módulo `asyncio` de Python proporciona una API asíncrona y no bloqueante para escribir código concurrente basado en eventos. `asyncio` usa corutinas y bucles de eventos para permitir la ejecución concurrente de tareas asíncronas, como la comunicación a través de sockets. En esta práctica utilizaremos `asyncio` para enviar y recibir datos UDP, llegando a intercambiar de esa manera documentos SDP.

## Objetivos de la práctica

* Conocer `asyncio` y cómo usarlo para enviar y recibir datos UDP.
* Repasar SDP, usándolo en paquetes UDP
* Utilizar el formato JSON

## Ejercicio 1. Creación de repositorio para la práctica

Con el navegador, dirígete al repositorio plantilla de esta práctica y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la EIF).

## Ejercicio 2. Prueba de asyncio

Prueba tu primer programa con `asyncio`:

```python
import asyncio

async def main():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')

asyncio.run(main())
```

En este programa tenemos una función asíncrona (`main`) que muestra las dos palabras de "¡Hola mundo!" en dos momentos, separados un segundo.

## Ejercicio 3. Empezando a entender asyncio

Para entender mejor cómo funciona asyncio, vamos a extender el ejercicio anterior. Mira los programas `hola1.py`, `hola2.py`, `hola3.py`, `hola4.py`, `hola5.py` y `hola6.py` del repositorio plantilla. Ejecútalos y estúdialos.

* `hola1.py`: Muy parecido al ejemplo anterior, pero se separa la creación de
 la corrutina (cuando se crea el objeto `co_hello`) y la ejecución de la misma
  (la llamada `asyncio.run(co_hello)`). Puede verse también cómo la corrutina
 devuelve su valor cuando es ejecutada, no cuando se crea.

* `hola2.py`: En este caso, vemos cómo una corrutina crea otra corrutina y
 permite que se ponga a ejecutar mediante un `await`. En realidad ya lo habíamos visto
 en el ejemplo anterior, con 'await asyncio.sleep(1)', pero ahora lo vemos explícitamente
 con `await co_other`. Vemos también cómo recoger el valor devuelto por una corrutina
 cuando se la ha activado con un `await`.

* `hola3.py`: En este caso, vemos qué se puede hacer con asyncio.gather, como forma
 de poner a ejecutar varias corrutinas a la vez, esperando a que terminen todas ellas.
 Tambien vemos cómo recoger el valor devuelto por las corrutinas en este caso.

* `hola4.py`: Vemos cómo las corrutinas pueden recibir un parámetro cuando se las
 crea, y cómo se trata este parámetro en su código.

* `hola5.py`: Ahora usamos un `asyncio.run()` para cada corrutina. Casa uno crea su
 propio gestor de eventos, donde solo tenemos la corrutina a ejecutar. Por tanto,
 esa corrutina se ejecuta hasta que termine, sin que la "siguiente" pueda ejecutarse,
 pues aún no ha sido puesta en un gestor de eventos (hasta que el siguiente `asyncio.run` se ejecuta).

* `hola6.py`: Muestra cómo podemos crear y dejar que se ejecuten las corrutinas
 "al vuelo", sin utilizar variables intermedias. Esta es una forma muy habitual
 de escribir este tipo de código. Se muestra con comentarios cómo habría sido
 un código más explícito.

Para entender mejor el código, es conveniente sabe que las funciones asíncronas devuelven una "corrutina", que es un objeto cuya finalización se puede esperar con `await`. Y que cada vez que esperamos con `await` estamos bloquándonos, dejando que otras tareas que pudieran estar listas para ejecutar utilicen el procesador.

Líneas como  `asyncio.run(main())` inician un gestor de eventos (un "bucle de ejecución"), que básicamente comienza a ejecutar la función asíncrona `main` hasta que se bloquee en un `await`, momento en que se ejecutará cualquier otra corrutina que esté lista para ejecutar, y así sucesivamente,

* `asynio.gather()` es una función asíncrona que espera a que terminen todas las funciones asíncronas que se le pasan como parámetro (en  caso, `hola()` varias veces). Puede verse por ejemplo en `hola3.py`


Referencias:

* [Python Asyncio Part 1 – Basic Concepts and Patterns](https://bbc.github.io/cloudfit-public-docs/asyncio/asyncio-part-1.html) en [CloudFit Public Docs](https://bbc.github.io/cloudfit-public-docs/
* [AsyncIO (Asynchronous I/O)](https://docs.python.org/es/3/library/asyncio.html), en español

## Ejercicio 4. Contadores asíncronos

Utilizando los mecanismos vistos hasta el momento, construye un programa, llamado `contadores.py` que lance varios contadores asíncronos. Cada contador será implementado por una función asíncrona que recibirá como parámetros un identificador de contador, y la cuenta de segundos que tiene que realizar (un entero):

```python
async def contador(id, time):
```

Cada contador mostrará un mensaje indicando que está empezando, un número con la cuenta de segundos (cada segundo) y un mensaje indicando que han terminado. Por ejemplo, si `id` es "A" y `time` es 4, el contador mostrará:

```shell
Contador A: starting...
Contador A: 1
Contador A: 2
Contador A: 3
Contador A: 4
Contador A: finishing...
```

Queremos que cuando funcionen varios contadores, cada uno vaya contando su tiempo en paralelo a los demás. Por ejemplo, si ejecutamos el contador de esta forma:

```python
async def main():
    await asyncio.gather(contador('A', 4), contador('B',2 ), contador('C',6))

asyncio.run(main())
```

Se mostrará en pantalla (las líneas con `[1seg]` no aparecen realmente, sino que representan el paso de un segundo; el orden entre A, B y C podría variar):

```python
Contador A: starting...
Contador B: starting...
Contador C: starting...
[1seg]
Contador A: 1
Contador B: 1
Contador C: 1
[1seg]
Contador A: 2
Contador B: 2
Contador B: finishing...
Contador C: 2
[1seg]
Contador A: 3
Contador C: 3
[1seg]
Contador A: 4
Contador A: finishing...
Contador C: 4
[1seg]
Contador C: 5
[1seg]
Contador C: 6
Contador C: finishing...
```

El objetivo final de este ejercicio es tener un fichero `contadores.py` que tenga las líneas Python indicadas en este ejercicio, que produzca la salida anterior, y que incluya una implementación de la función asíncrona `contador`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 4"


## Ejercicio 5. Contador con aviso

Para entender mejor algunos detalles de `asyncio`, seguimos explorando algunos ejemplos. En este caso, vamos a construir un contador sencillo, que cuente de 1 a 5, y avise cuando llega a 3. Pero lo vamos a hacer de varias formas, para explorar sobre todo cómo podemos comunicar información entre corrutinas, y cómo podemos sincronizarlas (que una de ellas haga algo después de que algo haya ocurrido en otra)

* `contador1.py`: El programa principal lanza `main()`, que a su vez lanza `contador()`, una corrutina que cuenta de 1 a 5 y avisa cuando llega a 3. `main()` espera a que `contador()` termine.

* `contador2.py`: El programa principal lanza `main()`, que ahora lanza dos corrutinas "en paralelo", `contador()` y `avisada()`, que es una corrutina que queremos que reciba un aviso cuando `contador()` llegue a 3. Para realizar el aviso, `main()` crea un "futuro", que pasa como parámetro a las dos corrutinas. `avisada()` se quedará esperándolo, y `contador()` le da un valor a llegar a 3.

* `contador3.py`: El programa principal lanza `main()`, que ahora lanza las dos corrutinas `contador()` y `avisada()`, les pasa a ambar un futuro (igual que en `contadore.py`), y se queda esperando a que el futuro finalice. Nótese que `avisada()` también espera a que finalice el mismo futuro. Cuando esto ocurra, ambas corrutinas (`main()` y `avisada()`) continuarán su ejecución.

Los "futuros" son un objeto cuya finalización se puede esperar con `await`. Cuando los esperamos con `await`, estamos bloqueandos, dejando que otras tareas que pudieran estar listas para ejecutar utilicen el procesador. El futuro finaliza cuando se le da un valor (con `set_result()`) o se cancela (con `set_exception()`). En ambos casos, el await se resuelve con ese valor, y la corrutina que esperaba continúa a partir de ahí. Además, en un futuro puede haber varias corrutinas esperando: todas recibirán el valor de finalización cuando el futuro finalice.

Las "tareas" son objetos similares a las corrutinas, pero se pueden crear sin tener que esperar a que terminen (usando `asyncio.create_task()`). De esta forma, una corrutina puede crear varios flujos (similares a corrutinas) nuevos, que seguirán funcionando "en paralelo" (aunque sólo una de ellas estará activa en un momento dado). `asyncio.create_task()` devuelve un objeto "tarea", sobre el que se puede esperar con `await`, igual que si fuera una corrutina.

Crea un programa `contadores_aviso.py`, que tenga dos contadores (usando como código una única función asíncrona, como `contador()` de `contador2.py`), y dos corrutinas de espera (usando como código una única función asíncrona, similar a `espera()` de `contador2.py`), que sea avisada cuando cada uno de los dos contadores lleguen a 3. Esto es, cada contador avisará cuando llegue a 3, y en ese momento las dos corrutinas de espera seán avisadas, para que puedan continuar. Estas corrutinas de espera, después de ser avisadas una vez, quedarán otra vez esperando hasta ser avisadas una segunda vez, y luego ya terminarán.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 5"


## Ejercicio 6. Haciendo el bucle de ejecución explicito

La función `asyncio.run()` es muy cómoda de usar en muchos casos, ya que se encarga de crear un bucle (loop) de ejecución de tareas, de esperar a que todas las tareas gestionadas por el buble terminen, y de cerrar el bucle, terminando.

En este ejercicio vamos a explorar otra opción menos transparente, que nos permite gestionar el bucle. Es la función `asyncio.get_event_loop()`.

Tendrás que crear un fichero `hola_loop.py` que funcione como `hola3.py`, pero que en lugar de la invocación a `asyncio.run()` cree un bucle de eventos usando `asyncio.get_event_loop()`, luego ejecute ese bucle hasta que termine la función `main()`, y cuando lo haga, lo cierre.

Puedes explorar cómo funciona `asyncio.get_event_loop()` en la [sección sobre el bucle de eventos](https://realpython.com/async-io-python/#the-event-loop-and-asynciorun) del tutorial "Async IO in Python: A Complete Walkthrough", y en el ejemplo `hola7.py` (que es igual que `hola6.py`, pero usando `get_event_loop()`)

**Nota:** Hay otra función, `asyncio.get_running_loop()`, que funciona de forma muy similar a `asyncio.get_event_loop()`, aunque no crea un bucle de eventos si no existía ya. Esta es, por tanto, últil para obtener el bucle de eventos actualmente en ejecución, cuando ya sabemos que existe. Veremos que se usa frecuentemente en estos casos, en otros ejemplos más adelante. Nótese también que ambas pueden usarse para conseguir un bucle de eventos para varios objetivos, como ejecutar luego una corrutina (ver a continuación), crear un futuro (como se vio anteriormente), o crear un termindor (endpoint) de UDP, como veremos en el ejercicio siguiente.

**Nota:** Para ejecutar una corrutina, dado el bucle de eventos, podemos usar `loop.run_until_complete()`, como en `hola7.py`, o `loop.run_forever()`, si sabemos que no retorna.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 6".


## Ejercicio 7. Cliente / servidor UDP de eco 

Estudia los ficheros `udp_echo_client.py` y `udp_echo_server.py`, que implementan un cliente y servidor de eco sobre UDP. Observa cómo se comportan cuando los ejecutas, y lee su código tratando de entenderlo.

Realiza una captura de su actividad (solo los paquetes UDP que intercambian) y guárdala en el fichero `echo.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 7".

## Ejercicio 8. Cliente / servidor que intercambian SDP

Utiliza las facilidades que proporciona el paquete [sdp-transform](https://github.com/skymaze/sdp-transform) para enviar y recibir documentos SDP sobre paquetes UDP. Para ello, utiliza el esquema del programa cliente / servidor del ejercicio anterior para escribir dos programas, `sdp_offer.py` y `sdp_answer.py`. El primero enviará una oferta SDP, y el segundo la recibirá, y devolverá al primero su respuesta SDP (en ambos casos, encapsuladas en un paquete UDP).

Para poder utilizar este módulo, tendrás que instalarlo previamente en un entorno virtual. Recuerda que puedes crear el entorno virtual desde PyCharm o desde el terminar. En el segundo caso, tendrás que activarlo antes de ejecutar programas Python en él. Por ejemplo:

```shell
python3 -m venv venv
source venv/bin/activate
```

La segunda línea tendrás que ejecutarla cuando quieras activar el entorno virtual `venv` previamente creado.

En cuanto a los documentos SDP a intercambiar, el que enviará `sdp_offer.py' tendrá la siguiente información: 

```shell
{'version': 0,
  'origin': {'username': 'user',
    'sessionId': 434222,
    'sessionVersion': 0,
    'netType': 'IN',
    'ipVer': 4,
    'address': '127.0.0.1'},
  'name': 'Session',
  'timing': {'start': 0,
    'stop': 0},
  'connection': {'version': 4,
    'ip': '127.0.0.1'},
  'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                     {'payload': 96, 'codec': 'opus', 'rate': 48000}],
             'type': 'audio',
             'port': 54400,
             'protocol': 'RTP/SAVPF',
             'payloads': '0 96',
             'ptime': 20,
             'direction': 'sendrecv'},
            {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                    {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
             'type': 'video',
             'port': 55400,
             'protocol': 'RTP/SAVPF',
             'payloads': '97 98',
             'direction': 'sendrecv'}]}
```

El que enviará `sdp_answer.py` será igual, pero cambiando los dos campos `port` que aparecen, que pasarán a ser `34222`.

Realiza también una captura de los paquetes UDP intercambiados por ambos programas (y sólo esos paquetes), y guárdala en el fichero `sdp.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 8".

## Ejercicio 9. SDP encapsulado en JSON

Realiza dos programas, `sdpjson_offer.py` y `sdpjson_answer.py` que funcionen como los descritos en el ejercicio anterior, pero en lugar de enviar el documento SDP directamente encapsulado sobre UDP, lo enviarán como parte de un documento JSON. Este documento JSON tendrá dos claves (propiedades): `type` y `sdp `. Los valores (datos) de estas claves serán:

* "type": tipo de documento ("offer" o "answer")
* "sdp": documento sdp como cadena de texto (striong)

Los documentos SDP que intercambiarán serán los mismos que en el ejercicio anterior.

Para realizar estos programas, utiliza el módulo `json` de la biblioteca estándar de Python.

Realiza también una captura de los paquetes UDP intercambiados por ambos programas (y sólo esos paquetes), y guárdala en el fichero `sdpjson.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 9".

## Ejercicio 10 (segundo periodo)

Para realizar esta práctica, tendrás que comenzar creando (si no lo has hecho antes) un nuevo grupo en el GitLab de la EIF, como se describe en el ejercico 11 de la práctica [01-entorno](../01-entorno/ejercicios.md).

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del repositorio plantilla de esta práctica uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la EIF de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `03-asyncio-<alumno>`.

Crea un clon local de este nuevo repositorio, y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Llama a esta rama `nueva` (ver detalles en la práctica `01-entorno`). 

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Crea en ella un nuevo programa, llamado `sdpjson_offer-<alumno>.py` que envíe una oferta con un documento SDP igual al original, pero cambiando el nombre de la sesión a `Segunda`. Prueba que al enviarlo, el programa de tu compañero te devuelve un documento SDP igual, pero con los puertos cambiados (como indica el enunciado del Ejercicio 10).

Cuando termines, crea un commit que incluya este nuevo fichero, y que tenga un comentario cuya primera línea sea "Ejercicio 11". Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes usar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).

## ¿Qué se valora de esta la práctica?

En está práctica valoraremos sobre todo que el código desarrollado realice lo indicado con la mayor exactitud posible. Además, la legibilidad del código, y en general el seguimiento estricto de las instrucciones serán tenidos en cuenta.
