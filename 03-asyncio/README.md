## AsyncIO, SDP y UDP

Actividades:

* Entrada / salida asíncrona en Python: asyncio
* Gestión de documentos SDP en Python: sdp-transform
* Envío y recepción de paquetes UDP en Python usando asyncio
* Descripción del ejercicio de entrega

Referencias:

* [asyncio](https://docs.python.org/3/library/asyncio.html)
* [Async IO in Python: A Complete Walkthrough](https://realpython.com/async-io-python/)
* [sdp-transform](https://github.com/skymaze/sdp-transform)

## Ejercicios

[Ejercicio de entrega evaluable (minipráctica)](ejercicios.md)

