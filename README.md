# PTAVI: Protocolos de Transmisión de Audio y Video en Internet

En este repositorio están organizados los materiales utilizados en las prácticas de la asignatura.

## Programa

* [Python y entorno de desarrollo](01-entorno)
  * 13 de septiembre
  * 20 de septiembre
  * 27 de septiembre

* [RTP y Wireshark](02-rtp)
  * 4 de octubre
  * 11 de octubre

* [AsyncIO, SDP y UDP](03-asyncio)
  * 18 de octubre
  * 25 de octubre

* [Introducción a WebRTC](04-webrtc-intro)
  * 8 de noviembre
  * 11 de noviembre

* WebRTC audio y video
  * 15 de noviembre
  * 29 de noviembre

* Proyecto final
  * 13 de diciembre
  * 20 de diciembre
