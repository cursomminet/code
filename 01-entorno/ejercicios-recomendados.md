# Protocolos para la transmisión de audio y video en Internet
# Python y entorno de desarrollo. Ejercicios recomendados.

**Objetivos:** Conocer el funcionamiento básico de git,
y las operaciones típicas que tendremos que realizar con él
durante las prácticas de la asignatura.

**Conocimientos teóricos previos:** Conocimientos básicos
de manejo del intérprete de comandos.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 1 hora.

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2024-2025/01-entorno-recomendado

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Introducción

Git es un sistema de control de versiones distribuido,
que permite gestionar los cambios a un conjunto de ficheros (repositorio),
y sincronizarlos con otros repositorios remotos. Lo utilizaremos en las prácticas
de la asignatura en combinación con GitLab, y lo utilizaremos tanto desde el
intérprete de comandos (terminal) como desde el IDE (PyCharm).

## Ejercicio 1

Comprueba que puedes entrar en tu cuenta del [GitLab de la EIF](https://gitlab.eif.urjc.es) (esto es, que puedes autenticarte o logearte).

Las acciones que se indican más adelante sobre GitLab, se refieren a este GitLab de la EIF. Normalmente, para hacer lo que se pide, habrá que estar autenticado (logeado) en él.

## Ejercicio 2

Realiza un fork del repositorio https://gitlab.eif.urjc.es/ptavi/pruebas
de forma que quede como uno de tus repositorios personales en el GitLab de la
EIF.

A continuación, clona este repo que acabas de crear en tu ordenador.
Edita el fichero `README.md` que verás en él, haciendo cualquier cambio, y crea un 
fichero nuevo llamado `fichero`.
Haz un commit que incluya todos estos cambios, y sincroniza el repositorio
que creaste en GitLab para que tenga también este commit.
Comprueba que el repositorio en GitLab tiene los cambios de tu repositorio
local.

## Ejercicio 3

Clona el mismo repositorio que has creado en tu cuenta en GitLab, pero
ahora en un directorio en el laboratorio (o en otro directorio en tu ordenador).
Haz cualquier cambio en este repositorio, y haz un commit que lo incluya.
Sube este commit al repositorio en GitLab, y sinconiza de nuevo tu primer
repositorio local para que tenga este cambio.

## Ejercicio 4

Haz cambios en ambos repositorios locales, en ficheros distintos, y trata de que
aparezcan en el otro repositorio local, y en el repositorio en GitLab.


## Ejercicio 5

Con el navegador, dirígete al repositorio plantilla de esta práctica (ver al comienzo de este enunciado)
y realiza un "fork" del mismo en tu cuenta, de manera que consigas tener una
copia del repositorio en tu cuenta. Clona en tu ordenador
local el repositorio que acabas de crear, para poder editar los archivos localmente.
Trabaja a partir de ahora en ese repositorio, que llamaremos "directorio de entrega". Sincroniza
los cambios (commits) que vayas efectuando según los ejercicios que se
detallan a continuación. Recuerda subir estos cambios a tu repositorio en
GitLab (normalmente, usando `git push`) para que queden reflejados allí.


## Ejercicio 6

Trabaja un poco con ficheros y directorios con el intérprete de comandos:

* Cámbiate al directorio de entrega. Crea en él un directorio con `mkdir`, cámbiate a él.
* Crea un fichero con el editor básico de Ubuntu, en ese directorio que acabas de crear, con el contenido "Hola", y que tenga el nombre `hola.txt`.
* Ejecuta los comandos `pwd`, `ls`, `ls -l` y `ls -al`.
* Crea, en la carpeta clonada a partir del repositorio plantilla, un fichero de texto plano, con el nombre "comandos.txt" con el resultado de ejecutar los comandos anteriores. Para cada ejecución, en ese fichero habrá una línea con el comando ejecutado, y lueg las que corresponda, generadas por el comando. Puedes editar el fichero con cualquier editor de texto plano, incluyendo, por ejemplo, PyCharm.
* Para rellenar ese fichero, copia y pega desde el intérprete de comandos el resultado de ejecutar esos comandos. Recuerda que en el intérprete de comandos puedes copiar un contenido seleccionándolo y pulsando "Mayúsculas"-"Control"-C (y no "Control"-C como se hace en otras aplicaciones). Luego podrás pegar lo copiado en el editor que estés usando, con "Control"-V. También puedes usar tanto en el intérprete de comandos como en el editor la acción de, una vez seleccionado el bloque de interés, pulsar el botón derecho y usar la opción de menú "Copiar" y luego "Pegar" en el editor.
* Haz un commit que incluya ese fichero, súbelo (`git push`) a tu repositorio en GitLab (el que creaste como fork del repositorio plantilla).

# Ejercicio 7

Realiza un programa para realizar operaciones matemáticas simples.

* Abre con PyCharm el directorio de entrega como un proyecto.
* Crea en ese directorio de entrega, usando, PyCharm un fichero, `operaciones.py`
* Escribe un programa que lea de la línea de comandos tres argumentos, que serán una operación de calculadora. El primer argumento será un número, el segundo será una operación, y el tercero será otro número. El programa escribirá en la línea de comandos el resultado de aplicar esa operación a los dos números. Las operaciones que se podrán escribir serán `+` (suma), `-` (resta), `*` (multiplicación) y `/` (división).
* Un ejemplo de ejecución es el siguiente:
```commandline
python3 operaciones.py 3.5 + 4
7.5
```
* Ejecútalo, y usa el depurador con él
* Haz un commit que incluya ese fichero, súbelo (`git push`) a tu repositorio en GitLab (el que creaste como fork del repositorio plantilla).

# Ejercicio 8

Realiza un programa que haga lo mismo del anterior, pero ahora estructurado en dos ficheros: un módulo `funciones.py` que tendrá una función para cada operación, y un programa principal `foperaciones.py` que cuando se ejecute utilizará el módulo `funciones.py` para hacer exactamente lo mismo que el anterior.

* Cada función aceptará dos argumentos (los dos números necesarios para hacer la operación). Las funciones se llamarán `suma`, `resta`, `multi` y `divi`. Un ejemplo de signatura de la función será:
```python
def suma(op1: float, op2: float) -> float
```
* Ejecútalo, y usa el depurador con él
* Haz un commit que incluya ese fichero, súbelo (`git push`) a tu repositorio en GitLab (el que creaste como fork del repositorio plantilla).

# Ejercicio 9

Asegúrate, comprobando en el GitLab de la EIF, que todos tus cambios están en tu repositorio de entrega de la práctica.
