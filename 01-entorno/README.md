## Python y entorno de desarrollo

Actividades:

* Presentación: los laboratorios Linux de la Escuela.
* Creación y actualización de cuentas para los laboratorios Linux (uso de móvil o cuenta alumno / EIF).
* Prueba de cuenta en uno de los ordenadores.
* Presentación: Linux, Ubuntu, GNOME.
* Presentación del escritorio Ubuntu:
  * Entorno de ventanas, lanzamiento de aplicaciones, entrada y salida.
  * Seguimiento de la pantalla compartida del profesor.
  * Entornos de trabajos (workspaces).
  * El gestor gráfico de ficheros
  * El sistema de ficheros:
    * Parte compartida (ejemplo: `/home`)
    * Parte de la máquina (ejemplos: `/tmp`, `/usr`)

Referencias:

* [Sitio web de los laboratorios docentes de la Escuela](https://labs.eif.urjc.es/)
* [Ubuntu Desktop Guide: Your desktop](https://help.ubuntu.com/stable/ubuntu-help/shell-overview.html.en)
* [Ubuntu Desktop Guide](https://help.ubuntu.com/stable/ubuntu-help/)

## Introducción a la shell (intérprete de comandos)

Actividades:

* Lanzamiento de una consola con intérprete de comandos
* Presentación: el intérprete de comandos de Linux (shell):
  * el prompt
  * `pwd`
  * ejecución de comandos (`fortune`, `date` como ejemplo)
  * flechas (adelante, atrás)
  * `ls`, `ls -l`, `ls -a`, `ls -al`
  * tabulador (completar)
  * `cd` (directorios, `..`, `.`, `~`)
  * `mkdir`, `rmdir`
  * `rm`
  * `du -s ~`
  * `man ls`
* Pestañas de la consola
* Interrumpir un comando: `<CTRL> c`. Ejemplo:
```
ls -R /
<CTRL> c
```
* Copiar y pegar en la consola: `<CTRL> <SHIFT> c`, `<CTRL> <SHIFT>v`
* Lanzamiento del intérprete de Python, y salida
* Python como calculadora


Referencias:

* [The Linux command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
* [Linux Command Line Full course](https://www.youtube.com/watch?v=2PGnYjbYuUo) (video)

## El IDE (Integrated Development Environment): PyCharm

Actividades:

* Lanzamiento de PyCharm
* Apertura de un proyecto
* El terminal de PyCharm
* El intérprete de Python de PyCharm. Python como calculadora (2)
* Primer script de Python
* Ejecución
* El depurador

Referencias:

* [PyCharm: Get Started](https://www.jetbrains.com/help/pycharm/quick-start-guide.html#code-assistance)
* [PyCharm: First Steps](https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html)
* [PyCharm: Debugging Python Code](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.html)

## Git

Actividades:

* Clonado de repositorio: `git clone url`
* Añadir nuevos ficheros al repositorio local: `git add fich`
* Crear una nueva versión (commit) en el repo local: `git commit .`
* Subir la nueva versión al repo remoto: `git push`
* Comprobar (en GitLab) que todo ha subido bien.

Referencias:

* [Pro Git](https://git-scm.com/book/en/v2). Libro sobre git, que cubre desde el uso básico hasta varios detalles avanzados, incluyendo el uso con GitHub (y GitLab).

* [Oh My Git!](https://blinry.itch.io/oh-my-git). Juego de cartas para aprender detalles de git, incluyendo cómo entender el grafo de versiones.

## Distintas formas de ejecutar un programa Python

Vamos a explorar distintas formas de ejecutar un programa Python

Comenzaremos con un programa que use el módulo 'turtle':

* Ejecución en sitio web

* Ejecución desde terminal, invocando con python:
  * Crea un fichero con cualquier editor de texto plano
  * Cámbiate al directorio donde está, y ejecútalo invocándolo con python:
```commandline
cd <directorio_donde_está_el_fichero>
python3 simple.py
```

* Ejecución desde terminal, directamente:
  * Añade la primera línea para ejecución:
```python
#!/usr/bin/python3
```

```python
#!/usr/bin/env python3
```
  * Da permisos de ejecución al programa (en línea de comandos, o mediante gestor gráfico de ficheros):
```commandline
cd <directorio_donde_está_el_fichero>
chmod u+x simple.py
```

  * Ejecuta directamente:
```commandline
./simple.py
```

Ahora, vamos a incluir un poco de interacción con el usuario:

* Ejecuta desde el terminal, preguntando al usuario:
  * Haz todo lo necesario y...
```commandline
python3 simple_input.py
```

* Ejecuta desde PyCharm
  * Se lanza PyCharm
  * Se abre un nuevo proyecto (opción "Open")
  * Se crea un fichero simple_input.py en él
  * Se ejecuta el fichero

* Ejecuta desde el terminal de PyCharm

* Ejecuta desde el intérprete de Python de Pycharm

* Ejecuta en el depurador de PyCharm

## Asistentes de IA

* [Codeium](https://codeium.com/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/20540-codeium-ai-autocomplete-and-chat-for-python-js-java-go--)
* [Cody](https://sourcegraph.com/cody)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/9682-cody-ai-coding-assistant-with-autocomplete--chat)
* [Continue](https://docs.continue.dev/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/22707-continue)
* [GitHub Copilot](https://github.com/features/copilot)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/17718-github-copilot)
* [CodeBuddy](https://codebuddy.ca/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/22666-codebuddy)
* [TabNine](https://www.tabnine.com)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/12798-tabnine)
    
## Testing

Crea un proyecto sencillo, con un programa que tenga un par de funciones.

Usa `unittest` para probar el programa. Puedes hacer algún test con el asistente de IA.

## Documentación, estilo

En el programa anterior, crea documentación para las funciones, como
docstrings en las propias funciones. Si quieres, utiliza la ayuda del
asistente de IA para ello.

Comprueba el estilo del programa con `pydocstyle`. Utiliza la opción
"Code | Reformat Code" de PyCharm para reformatear el código, de forma
que cumpla más fácilmente la guía de estilo.

## Ficheros para el CI de GitLab

Observa el fichero `.gitlab-ci.yml`. Pruébalo, de forma que consigas que
el CI de GitLab ejecute tus tests, y comprueba que tu programa cumple
la guía de estilo, usando `pycodestyle`.

# Entornos virtuales

Crea un entorno virtual com `python3 -m venv venv`.

Actívalo con `source venv/bin/activate`.

Instala en él  algún paquete con `pip install`, y comprueba que
ha sido instalado.

Referencias:

* [How to Run Your Python Scripts and Code](https://realpython.com/run-python-scripts/)
* [PyCharm: Run and rerun applications](https://www.jetbrains.com/help/pycharm/running-applications.html)
* [Python if elif else](https://www.datacamp.com/tutorial/python-if-elif-else)
* [Unittest tutorial](https://docs.python.org/3/library/unittest.html)
* [Pep8](https://peps.python.org/pep-0008/)
* [Pydocstyle](https://pydocstyle.readthedocs.io/en/latest/)
* [Python virtual environments and packages](https://docs.python.org/3/tutorial/venv.html)

## Ejercicios

[Ejercicios recomendados](ejercicios-recomendados.md)

[Ejercicios de entrega evaluable (minipráctica)](ejercicios.md)

