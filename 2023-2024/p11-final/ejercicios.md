# Protocolos para la transmisión de audio y video en Internet
# Práctica 11. Proyecto final

**Nota:** Esta práctica constituye el proyecto final de la asignatura. Es preciso aprobarla para poder aprobar la asignatura. Para la evaluación
de esta entrega se valorará su correcto funcionamiento según los requisitos y
guías indicados. La puntuación del proyecto será como sigue:

* Funcionamiento correcto de la parte básica: 0.1 puntos.
* Funcionamiento correcto de la parte adicional: hasta 1.9 puntos, dependiendo de las opciones realizadas.
* Funcionamiento correcto de la parte colectiva: hasta 0.75 puntos, dependiendo de lo que se realize.

**Conocimientos previos necesarios:**

Todos estos conocimientos necesarios para la realización de este proyecto se han trabajado en prácticas previas, y en los temas de teoría. Entre ellos, los más importantes son:

* Nociones de uso de wireshark
* Nociones de XML y JSON
* Nociones de UDP y asyncio
* Nociones de RTP, SDP, y protocolos de señalización
* Nociones de WebRTC y aiortc

**Tiempo estimado:** 20 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2023-2024/p11-final

**Fecha de entrega (partes básica, adicional y colectiva):** 18 de enero de 2023, 23:59

**Entrevistas sobre prácticas:** 19 de enero de 2023, a continuación del exámen (en horario por determinar)

## Introducción

Este proyecto tiene como objetivo implementar un servicio distribuido de transmisión de audio y video, utilizando WebRTC. En su parte más básica, será fundamentalmente un servicio de streaming, donde un conjunto de servidores ofrecerán videos que podrán ser vistos desde navegadores, utilizando un servidor "central" para que los servidores puedan ofrecer los videos que transmiten, y los clientes puedan consultar qué videos desean ver. Este servidor central funcionará también como servidor de señalización. En su parte adicional, se podrá añadir funcionalidad diversa: realización de procesamiento previo de los videos ofrecidos, inclusión de la videoconferencia como servicio, mejoras en el registro de servidores y clientes, etc. en su parte colectiva, se trabajará fundamentalmente en la interoperabilidad entre soluciones realizadas por distintos alumnos.

## Comienzo

Con el navegador, dirígete al repositorio plantilla de este proyecto y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según vayas avanzando en tu proyecto (haciendo commit y subiéndolo a tu repositorio en el GitLab de la EIF).

## Parte básica: requisitos

El sistema que constituye la parte básica de la asignatura consistirá de los siguientes programas:

* Un servidor de streaming de audio y video, capaz de enviar mediante WebRTC un video, indicado como argumento en la línea de comandos, a los navegadores.

* Un servidor de señalización, que se encargará de recibir información de los servidores de streaming y del frontal web, mediante paquetes UDP, y los pondrá en contacto.

* Un servidor web (frontal web) que permitirá que los navegadores vean los videos disponibles (porque los sirve algún servidor de streaming). Para hacer su trabajo, este servidor web intercambiará paquetes UDP de señalización con el servidor de señalización, y ofrecer-a una página HTML a los navegadores, en la que se podrá acceder al catálogo de videos, y se podrá elegir cuál de ellos se quiere ver.

### Lanzamiento de los programas

El modo de funcionamiento normal será el siguiente:

* Se lanza el servidor de señalización, indicando en qué puerto UDP escuchará mensajes de señalización de las demás partes:

```commandline
python3 signaling.py <signal_port>
```

* Se lanzan varios servidores de streaming de audio y video. Cada uno recibirá como argumentos en la línea de comandos el nombre del fichero de video que servirá, y la dirección IP y el puerto del servidor de señalización:

```commandline
python3 streamer.py <file> <signal_ip> <signal_port>
```

* Se lanza el servidor web (frontal web) que servirá la página HTML que permitirá a los navegadores que elijan la película, y que se conectará al servidor de señalización para poder saber qué streamers hay activos, y enviarles señalización para contactar con ellos. Recibirá como argumentos el puerto TCP donde atenderá peticiones HTTP, y la dirección IP y el puerto del servidor de señalización:

```commandline
python3 front.py <http_port> <signal_ip> <signal_port>
```

### Estructura de cada programa

Cada uno de los programas anteriores estará compuesto por tantas funciones, clases y otros elementos Python como sea conveniente. Pero todos ellos tendrán la misma estructura general:

```python
...

def main():
    ...
    
if __name__ == '__main__':
    main()
```

Los programas que hayan de utilizar comunicación mediante UDP lo harán usando las facilidades que proporciona el módulo `asyncio` de Python para ello. Para comunicarse mediante WebRTC, usarán el módulo `aiortc`. Para proporcionar una interfaz HTTP que permita servir páginas HTML a los navegadores, usarán el módulo `aiohttp`.

### Funcionamiento específico: signaling.py (servidor de señalización)

Al arrancar, el programa quedará esperando mensajes UDP en el puerto indicado. En líneas generales estos mensajes deberán ser al menos de estos tipos:

* Registro de streamer. Estos mensajes llevarán al menos el nombre del fichero que sirve el streamer. Utilizando el puerto de origen, el programa podrá saber también desde qué dirección IP y puerto UDP el streamer envió el mensaje (que será también donde esté escuchando nuevos mensajes UDP).

* Petición de lista de ficheros. Enviado por un servidor frontal, este mensaje se enviará para obtener la lista de ficheros que hay ahora mismo registrados con el programa.

* Oferta de navegador. Llevará información de un navegador qeu quiere recibir un cierto fichero, e incluirá información de una oferta SDP, parte del establecimiento de comunucación WebRTC. Este mensaje tendrá su origen normalmente en un navegador que quiere recibir ese fichero. Normalmente, el programa recibirá estas ofertas del servidor frontal.

* Respuesta de streamer. Llevará información de una respuesta SDP, parte del establecimiento de comunucación WebRTC, enviada por un streamer en respuesta a una oferta que ha recibido.

Además, se podrán atender otros mensajes, y se podrán definir los mensajes que se envían según las necesidades del sistema.

En general, cuando se reciban mensajes de registro, el programa almacenará esa información en una estructura de datos en memoria, que llamaremos "directorio", que  permitirá saber en qué direcciones IP y puerto UDP está escuchando mensajes de señalización el streamer que sirve un fichero de un cierto nombre. Esta información será utilziada para responder mensajes de petición de lista de ficheros, y para localizar a un determinado streamer cuando llegue una oferta de navegador para él.

Cuando se reciba un mensaje de oferta, se cursará al streamer correspondiente, y cuando se reciba una respuesta de un streamer, se cursará al servidor frontal para que el navegdor correspndiente pueda establecer la comunicación WebRTC.

Mensajes históricos mínimos:

* Comienzo. Cuando comienza la ejecución del programa.
* Recepción de registro. Cuando se registra un streamer. Se mostrará también la dirección IP y el puerto de ese streamer.
* Recepción de oferta. Cuando se ha recibido una oferta (con el documento SDP) del servidor frontal (enviada originalmente por de un navegador).
* Envío de oferta. Cuando se ha cursado una oferta, enviándola al streamer correspondiente.
* Recepción de respuesta. Cuando se ha recibido la respuesta (con el documento SDP) de un streamer.
* Envío de respuesta. Cuando se ha enviado la respuesta (con el documento SDP) al servidor frontal, destinada a un navegador.


### Funcionamiento específico: streamer.py (streamer)

Cuando el programa arranque enviará un mensaje de registro al servidor de señalización, indicando cuál es su fichero. A partir de que se haya registrado, estará atento a mensajes de oferta con información SDP, para iniciar una conexión WebRTC directamente con el navegador que quiere recibir su fichero. Por esa conexión, enviará el fichero que sirva.

Mensajes históricos mínimos:

* Comienzo. Cuando comienza la ejecución del programa.
* Recepción de oferta. Cuando se ha recibido una oferta (con el documento SDP) del servidor de señalización (enviada originalmente por de un navegador).
* Envío de respuesta. Cuando se envía la respuesta (con el documento SDP) al servidor de señalización.
* Inicio de comunicación WebRTC. Cuando se ha establecido el canal WebRTC con el navegador.

### Funcionamiento específico: front.py (servidor frontal)

Cuando el programa arranque enviará un mensaje de petición de directorio al servidor de señalización, y utilizará esa información para preparar la página HTML que ofrecerá a los navegdores (que será el recurso principal, '/' en el puerto TCP indicado como argumento al lanzarlo). Esta página mostrará un listado de los nombres de los ficheros disponibles en los streamers. Junto a cada nombre de fichero se mostrará un botón que permitirá empezar a ver ese video.

Para que el navegador pueda ver un video, al pulasr ese botón el servidor frontal recibirá una petición HTTP (por ejemplo, un `POST`). En esa petición el servidor frontal recibirá una oferta WebRTC con información SDP del navegador que quiere establecer la comunicación. Al recibirla, el servidor frontal la cursará al servidor de señalización. De igual manera, cuando el servidor frontal reciba una respuesta a esta oferra, la cursará al navegador, como respuesta a esa petición HTTP, de forma que este pueda establecer la comunicación WebRTC para empezar a recibir el fichero.

Mensajes históricos mínimos:

* Comienzo. Cuando comienza la ejecución del programa.
* Petición de directorio. Cuando se ha pedido el listado de videos del directorio, al servicor de señalización
* Recepción de directorio. Cuando se ha recibido el listado de videos del directorio, desde el servicor de señalización
* Listado. Cuando se ha servido un listado de ficheros a un navegador.
* Recepción de oferta. Cuando se ha recibido la oferta (con el documento SDP) de un navegador.
* Envío de oferta. Cuando se ha enviado al servidor de señalización.
* Recepción de respuesta. Cuando se ha recibido la respuesta (con el documento SDP) de un streamer.
* Envío de respuesta. Cuando se ha enviado la respuesta al navegador que la estaba esperando.

### Capturas y ficheros SDP a entregar

Se tendrán que ejecutar dos escenarios, y en cada uno de ellos se almacenarán capturas, y documentos SDP de las ofertas de los navegadores, y de las respuestas de los streamers. Los escenarios serán los siguientes:

* Escenario 1. Se lanzará el servidor de señalización, un servidor de streaming, y el servidor frontal. A continuación, desde un navegador se cargará la página con el listado, ofrecida por el servidor frontal, se elegirá un video, y se verá. La captura terminará cuando el video haya comenzado a verse en el navegador (Se incluirán al menos 10 paquertes del envío del fichero).

* Escenario 2. Igual que antes, pero ahora se lanzarán dos streamers y dos navegadores, y en cada navegador se descargará un solo fichero de un srteamer (distinto al que descargue el otro navegador).

En ambos casos, al captura deberá incluir solo paquetes generados por los programas del sistema, incluido el navegador (en lo que tenga que ver con el funcionamiento del sistema).

Los ficheros de las capturas se nombrarán `captura1.libcap` y `captura2.libcap` (según el escenario al que correspondan). Los ficheros con los documentos SDP del primer escenario se nombrarán `1_navegador.sdp` y `1_streamer.sdp`, y los del segundo escenario `2_navegador-<numero>.sdp` y `2_streamer-<numero>.sdp`, donde `<numero>` será 1 o 2 según sea el primer o segundo navegador o streamer que se han ejecutado.

### Formato de ficheros históricos (logs)

Para los mensajes históricos de ejecución de un programa se utilizará un formato similar al siguiente, donde el primer elemento de cada línea es la fecha, hora, minuto, segundo y milisegundo, y luego el mensaje relevante a lo que está haciendo el programa, según lo indicado más arriba, en la descripción de cada programa.

```
20211128152045 Comenzando
20211128153012 Mensaje REGISTRO recibido de 127.0.0.1 2354
20211128153017 Mensaje OK enviado a 127.0.0.1 2354
20211128153036 Mensaje de oferta SDP recibido de 127.0.0.1 4567
```

Los mensajes históricos se escribirán en la salida de error (`stferr`) del programa.

## Parte adicional

* Persistencia: el servidor de señalización utilizará un fichero XML para guardar la informacipón de su directorio. Cuando arranque el servidor de señalización, buscará si existe ese fichero, y en caso de que sea así, leerá sus contenidos para inicializar su directorio. Este fichero XML tendrá siempre el mismo nombre, conocido por el servidor de señalización (lo tendrá en su código).

* Varios servidores frontales: El sistema admitirá que haya varios servidores frontales, y que los navegadores utilicen cualquiera de ellos, con los mismos efectos.

* Información de vida: El sistema tendrá algún sistema para detectar cuándo los streamers siguen vivos, y los quitará del directorio cuando no sea así. Además, esta infroamción se propagará al servidor frotnal, de forma que si un servidor de streaming deja de estar en el directorio, ya no será ofrecido a los navegadores.

* Uso de SIP: Para los mensajes de señalización se podría usar un subconjunto de SIP (o al menos, algo parecido a SIP). En esta opción, se realizará el protocolo de señalización de forma que Wireshark reconozca al menos parcialmente los mensajes utilizados como mensajes SIP.

* Información adicional para los ficheros: Cuando los streamers se registren, enviarán al servidor de señalización información adicional. Por ejemplo, esta información puede incluir un título y un resumen en texto del fichero, y una imagen que lo ilustre. Esta información se almacenará en el directorio del servidor de señalización, que se la pasará al servidor frontal cuando éste le pida un listado, para poder mostrársela a los navegadores.

* Funcionamiento entre distintas máquinas. En este caso, cada uno de los componentes del sistema (los tres programas, y el navegador) han de estar en máquinas diferentes (de forma que no se utilizará 127.0.0.1 como dirección, sino la real de la máquina). En caso de proponer esta opción como parte adicional, es fundamental incluir los ficheros de captura correspondientes a la prueba de ejecución.

## Parte colectiva

La parte colectiva consistirá en probar el funcionamiento de alguno o algunos de los programas del proyecto en un sistema donde también participen programas del proyecto de otro estudiante. Lo habitual en estos casos será que los estudiantes se pongan desde el principio de acuerdo en los mensajes a intercambiar, de forma que sus programas interoperen.

En caso de que se presente esta parte, es muy importante que el código de los programas no sea igual, y se valorará que sea lo más diferente posible (dentro de los razonable) entre los poryectos de los estudaints que presenten la parte colectiva.

La parte colectiva se presentará siempre como interoperación con el proyecto de otro estudiante, aunque los formatos de los mensajes pueden acordarse en grupos de más de dos estudiantes.

Si un estudiante A presenta una parte colectiva con el proyecto de otro estudiante B, no es preciso que ese otro estudiante B presente parte colectiva con el estudiante A. Pude presentarla con cualquier otro estudiante, si así lo prefiere, o no presentar parte colectiva.

La parte colectiva no tiene porqué funcionar con la versión final de otro proyecto: puede funcionar con un commit concreto: en ese caso se indicará con qué commit interopera.

Para mostrar que se entrega la parte colectiva habrá que especificar qué programas de qué proyecto se han lanzado, y con qué opciones. Además, habrá que aportar una captura del funcionamiento cojunto, y ficheros con los documentos SDP de navegadores y streamers (ver apartado de entrega, más abajo).

## Entrega de la práctica

La práctica se entregará en el repositorio del alumno en el GitLab de la EIF, que habrá sido derivado (haciendo "fork", como se indica al comienzo de este enunciado) del repositorio plantilla del proyecto final.

El repositorio deberá incluir los ficheros que había en el repositorio plantilla, más:

* Ficheros con los programas principales `signaling.py`, `streamer.py` y `fornt.py`, más los ficheros auxiliares que sean necesarios para que funcionen (por ejemplo, ficheros HTML y/o Javascript).

* Capturas indicadas en el apartado "Capturas y ficheros SDP a entregar", más arriba: `captura1.libpcap` y `captura2.libcap`

* Los ficheros correspondientes a los documentos SDP indicados en el apartado "Capturas y ficheros SDP a entregar", más arriba (en total, seis ficheros).

* Un fichero `requirements.txt`, con un nombre de paquete Python por línea, para indicar cualquier biblioteca Python que pueda hacer falta para que la aplicación funcione, si es que fuera el caso, incluento `aiortc` y `aiohttp`. Si es posible, se recomienda escribir este fichero en el formato  que entiende `pip install -r requirements.txt`.

* Cualquier fichero auxiliar que pueda hacer falta para que funcione la práctica, si es que fuera el caso.

Hay muchas herramientas que permiten realizar la captura de pantalla. Por ejemplo, en GNU/Linux puede usarse Gtk-RecordMyDesktop o Istanbul (ambas disponibles en Ubuntu) o quizás mejor OBS (disponible para varias plataformas como Linux, Windows y MacOS). Es importante que la captura sea realizada de forma que se distinga razonablemente lo que se grabe en el vídeo.  En caso de que convenga editar el vídeo resultante (por ejemplo, para eliminar tiempos de espera) puede usarse un editor de vídeo, pero siempre deberá  ser indicado que se ha hecho tal cosa con un comentario en el audio, o un texto en el vídeo. Hay muchas herramientas que permiten realizar esta edición. Por ejemplo, en GNU/Linux puede usarse OpenShot o PiTiVi.

* Fichero de entrega, `entrega.md`, en formato Markdown, con la siguiente información:

  - la primera línea del fichero ha de ser `# ENTREGA CONVOCATORIA ENERO`. La segunda, el nombre completo del estudiante, y su dirección de correo de la Universidad.

  - Sección sobre la parte básica (`## Parte básica`). Incluirá cualquier comentario relevante sobre la parte básica de la práctica, incluyendo cualquier aspecto que no funcione como se indica en el enunciado.

  - Sección sobre la parte adicional (`## Parte adicional`). Incluirá cualquier comentario relevante sobre la parte adicional de la práctica, incluyendo fundamentalmente el listado de las opciones que se han realizado (una por línea, comenzando por `* `, para que queden en un listado, y con el mismo contenido en esa línea que el nombre del apartado correspondiente paa esa opción). Por ejemplo:

```markdown
* Uso de SIP

    Explicación
  
* Información de vida

    Explicación
```

  Además de este listado incluirá una explicación (ver el ejemplo anterior) sobre cómo probar esa parte adicional, indicando al menos qué programas hay que lanzar, con qué argumentos, y cómo se puede comprobar que funciona el apartado en cuestión. Es coneniente entregar también ficheros de captura y (si es relevante) ficheros SDP obtenidos de la ejecutción. En esos casos, estos ficheros se nombrarán `opcion<numero>-<fichero>`, donde `<numero>` es un número secuencial para cada opción entregada, y `<fichero>` es el nombre de fichero, siguiendo más o menos la nomenclatura utilizada para los ficheros entregados como parte básica.

  - Sección sobre la parte colectiva, explicando en detalle con qué programas de qué proyecto se ha probado la parte colectiva, y las líneas de lanzamiento de todos los programas en el escenario de interoperación que se ha probado. Además, se almacenarán los ficheros de captura y de documento SDP del escenario, nombrados como `colectiva-<fichero>`, donde `<fichero>+` es el nombre del fichero de entrega de la práctica individual, tal y como se explica más arriba.

  Si se ha realizado cualquier parte adicional que no venga indicada específicamente en el enunciado, puede indicarse en este listado como `Otra:`, seguido de una descripción de la mejora (una entrada `Otra` por cada mejora).

  - Enlace al video de demostración del proyecto. El vídeo será de una duración máxima de 2:00 minutos si se presenta solo la parte básica, de 3:00 minutos si también se presenta la parte adicional, y de 4:00 minutos si se presenta también la parte colectiva. El video constará de una captura de pantalla que muestre cómo se lanzan las aplicaciones, cómo se ve el video desde el navegador, y en general que muestre lo mejor posible la funcionalidad del proyecto en uno de los escenarios de prueba. Siempre que sea posible, el alumno comentará  en el audio del vídeo lo que vaya ocurriendo en él. El video se subirá a algún servicio de subida de vídeos en Internet (por ejemplo, Vimeo, Twitch, o YouTube).

  - Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Es importante comprobar que este documento se ve correctamente desde la interfaz eb de GitLab (eso es, que el marcado Markdown se interpreta correctamente)

**Si este fichero no está correctamente en el repositorio, no se corregirá la práctica.**

## Entrevista sobre prácticas

Es importante recordar que para poder aprobar el proyecto final, y para que las prácticas de evaluación continua sean evaluadas, habrá que presentarse a la entrevista sobre prácticas. El objetivo de esta entrevista es asegurarse de que el estudiante comprende el código que ha entregado, y sabe cómo hacer modificaciones sencillas sobre él.

La entrevista consistirá en una conversación presencial en la que se pedirán explicaciones sobre el funcionamiento de las prácticas entregadas (y especialmente del proyecto final), su código fuente, y otros materiales entregados con ellas (como capturas de paquetes, por ejemplo). Como parte de esta entrevista se podrá pedir también la realización de cambios sencillos al código de cualquiera de las prácticas, para aclarar cualquier duda sobre el conocimiento del estudiante sobre ellas.

La entrevista se realizará en un laboratorio de la EIF, que se anunciará oportunamente, y el estudiante tendrá que tener todas sus prácticas, tal y como han sido entregadas, y listas para ser ejecutadas y explicadas.

## Preguntas y respuestas

