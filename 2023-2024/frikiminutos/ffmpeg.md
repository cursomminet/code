## FFmpeg

[FFmpeg](https://ffmpeg.org/) es una caja de herrameintas para grabar, convertir y transmitir videos, usando varios formatos y protocolos.

Utilidades principales: ffmpeg, ffprobe, ffplay.

Algunos ejemplos:

* Conversíón de formatos:

```commandline
ffmpeg -i video.mp4 video.avi
```

* Información sobre un fichero o una url de streaming:

```commandline
ffprobe video.mp4
ffprobe rtsp://localhost:8554/video
```

* Extracciñon de partes de un video:

```commandline
ffmpeg -ss 00:00:03 -i video.mp4 -t 00:00:05 -vcodec copy -acodec copy video2.mp4
```

* Extraer el audio de un video, y grabarlo en formato MP3:

```commandline
ffmpeg -i video.mp4 -vn -ar 44100 -ac 2 -ab 192k -f mp3 sound.mp3
```

* Convertir un video a un GIF animado:

```commandline
ffmpeg -i video.mp4 animated_gif.gif
```

* Extraer los cuadros (frames) de un video como imágenes:

```commandline
ffmpeg -i video.mp4 image%d.jpg
```

Puedes consultar [otros ejemplos interesantes](https://www.busindre.com/comandos_ffmpeg_utiles).
