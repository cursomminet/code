## GStreamer

GStreamer es un sistema bastante completo para tratamiento de audio y video, con muchas capacidades, formatos y protocolos soportados.

Algunos ejemplos:

```shell
# Suena un test de audio
gst-launch-1.0 audiotestsrc ! autoaudiosink
# Ruido blanco, volumen bajo
gst-launch-1.0 audiotestsrc wave="white-noise" volume=0.1 ! autoaudiosink
# Señal senoidal, 1000 Hz
gst-launch-1.0 audiotestsrc freq=1000 ! autoaudiosink
# Muestra un test de video
gst-launch-1.0 videotestsrc ! fakesink
# Genera un video de test en formato FLV, codificado en X.264
gst-launch-1.0 -vv videotestsrc ! x264enc ! flvmux ! filesink location=video.flv
# Muestra la salida de la cámara (parámetros por defecto)
gst-launch-1.0 v4l2src ! videoconvert ! ximagesink
# Muestra la salida de la cámara (eligiendo algunos parámetros)
gst-launch-1.0 v4l2src name=cam_src ! videoconvert ! videoscale ! video/x-raw,format=RGB ! queue ! videoconvert ! ximagesink name=img_origin
# Audio y video de prueba
gst-launch-1.0 -v audiotestsrc ! autoaudiosink videotestsrc ! autovideosink
# Audio y video a un fichero MP4
gst-launch-1.0 -v -e videotestsrc   ! x264enc   ! mp4mux name=mux   ! filesink location="video.mp4" audiotestsrc   ! lamemp3enc   ! mux.
# Información sobre un fichero multimedia
gst-discoverer-1.0 -v  video.mp4 
# Ver video y oir audio de un fichero MP4
gst-launch-1.0 filesrc location=video.mp4   ! qtdemux name=demux  demux.audio_0   ! queue ! decodebin ! audioconvert ! audioresample ! autoaudiosink  demux.video_0   ! queue ! decodebin ! videoconvert ! videoscale ! autovideosink
```

La mayoría de estos ejemplos están tomados de (o inspirados en) los que se pueden encontrar en [Gstreamer Pipeline Samples](https://gist.github.com/hum4n0id/cda96fb07a34300cdb2c0e314c14df0a)

Permite también cambiar formatos de video y audio (transcodificar), enviar por la red usando varios protocolos, y muchas cosas más.

Más información:

* [GStreamer](https://gstreamer.freedesktop.org)
* [Documentación sobre GStreamer](https://gstreamer.freedesktop.org/documentation/)
