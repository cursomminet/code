## Transcripción de voz (de voz a texto)

Transcribe a texto un fichero de audio con voz (en inglés, español, u otros idiomas): [transcribe.py](habla-texto/transcribe.py)

* Dependencias: [whisper](https://github.com/openai/whisper)

```commandline
pip install openai-whisper
```
