## ¿Cómo hemos recibido la televisión?

Durante los últimos años, la forma como hemos recibido televisión ha variado mucho:

[Televisión analógica](https://en.wikipedia.org/wiki/Analog_television) (primero blanco y negro, luego en color):

* Televisión analógica terrestre
* [Televisión analógica por cable](https://en.wikipedia.org/wiki/Cable_television) (CATV)
* [Televisión analógica por satélite](https://en.wikipedia.org/wiki/Satellite_television)

Televisión digital:

* [Televisión digital terrestre](https://en.wikipedia.org/wiki/Digital_terrestrial_television) (TDT). Ejemplo: [DVB-T](https://en.wikipedia.org/wiki/DVB-T), video en MPEG-2 y MPEG-4.
* [Televisión digital por cable](https://televisiondigital.mineco.gob.es/TelevisionDigital/formas-acceso/Paginas/tv-cable.aspx). Ejemplo: [DVB-C](https://en.wikipedia.org/wiki/DVB-C), video en MPEG-2 o MPEG-4. 
* [Televisión digital por satélite](https://televisiondigital.mineco.gob.es/TelevisionDigital/formas-acceso/Paginas/tv-satelite.aspx). Ejemplo: [DVB-S](https://en.wikipedia.org/wiki/DVB-S) y [DVB-S2](https://en.wikipedia.org/wiki/DVB-S2) , video en MPEG-2 y MPEG-4.
* [Televisión sobre IP](https://en.wikipedia.org/wiki/Internet_Protocol_television) (IPTV)