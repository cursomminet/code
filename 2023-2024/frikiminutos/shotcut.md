## Shotcut, editor de vídeo

Hay muchos editores de vídeo, u [Shotcut](https://shotcut.org/) es uno de los más populares. Está disponibile para muchas plataformas, es software libre y gratuito, y dispone de muchos módulos (plugins) para trabajar con diversos formatos de vídeo.

* [Repositorio de desarrollo](https://github.com/mltframework/shotcut)
