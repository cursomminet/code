## Descarga ficheros de video de un servidor de streaming

Puede probarse con el servicio [TV URJC](https://tv.urjc.es).

Basta con, cuando se está viendo el video por streaming en el navegador, abrir las herramientas del desarrollador, pestaña "Red", y buscar las intereracciones que descargan los videos (en este caso, son videos en formato MP4). Cuando se han identificado, se pueden, por ejemplo, abrir en otra pestaña, y luego se guardan.

Para identificar las interacciones que descargan los videos, se pueden ordenar las interacciones por tamaño: las que descargan videos suelen ser las más grandes. También pueden buscarse mime types que indiquen video, o identificar cuál es la máquina que sirve contenidos, y centrarse en la interacción con esa máquina.
