## Grabación de audio

Graba audio del micrófono: [sound_record.py](grabacion/sound_record.py).

* Dependencias: [sounddevice](https://python-sounddevice.readthedocs.io/en/0.4.5/usage.html#recording). Necesita también [scipy](https://scipy.org/).

```commandline
pip install sounddevice scipy
```
