# Protocolos para la transmisión de audio y video en Internet
# Práctica 9. RTSP y SIP

**Nota:** Esta no es una práctica entregable, aunque se recomienda su realización para afianzar los conocimientos sobre XML y JSON.

**Conocimientos previos necesarios:**

* Nociones de RTSP y SIP (presentación en teoría, y prácticas anteriores)

**Tiempo estimado:** 3 horas

## Introducción

Vamos a trabajar en esta práctica con los protocolos RTSP y SIP, para tener un conocimiento más profundo de ambos.

## Objetivos de la práctica

Tener una experiencia de primera mano con capturas de los protocolos RTSP y SIP.

## Ejercicio 1. Comunicación RTSP

Lanza el servidor multimedia [MediaMTX](https://github.com/bluenviron/mediamtx), envíale un canal de video vía RTSP, y consume ese mismo canal de vídeo también vía RTSP. Para enviar y consumir los canales de video, usa comandos de FFmpeg.

Comienza descargando el [programa binario mediamtx](https://github.com/bluenviron/mediamtx#standalone-binary). Para Linux, puedes descargar el fichero tar.gz para la arquitectura de tu ordenador (normalmente, amd64). Desempaquétalo con el comando `tar xvzf <fichero.tar.gz>`.

Ahora, ejecuta por separado (en terminales distintos) los comando spara lanzar el servidor, para subierle video, y para consumir video de él (usa el fichero `video.mp4`):

```
./mediamtx
```

```
ffmpeg -re -stream_loop -1 -i video.mp4 -c copy -f rtsp rtsp://localhost:8554/video
```

```
ffplay rtsp://localhost:8554/video
```

Cuando hayas visto el video en tu pantalla, cierra todos los programas, y pasa a tomar una captura del RTSP interecambiado. Para ello, lanza wireshark y empieza a capturar con él antes de volver a lanzar los tres programas.

Analiza la traza, y relaciona los paquetes RTSP con lo visto en clase.

## Ejercicio 2. Traza SIP

Utiliza una de las trazas que vimos en el tema de RTP, que también contienen paquetes SIP, para analizar el funcionamiento de SIP. Relaciona lo que veas con lo que hemos visto en clase.

