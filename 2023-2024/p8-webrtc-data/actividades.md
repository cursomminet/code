## WebRTC datos

Actividades:

* Uso de WebRTC para transmitir datos entre pares
* Descripción del ejercicio de entrega

**Ejercicio:** [Práctica 8. Envío de datos con WebRTC](ejercicios.md)

Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
