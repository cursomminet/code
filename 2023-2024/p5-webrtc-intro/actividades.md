## Introducción a WebRTC

Actividades:

* Prueba de una aplicación sencilla WebRTC
* Prueba de un módulo Python que implementa WebRTC

**Ejercicio:** Aplicación WebRTC sencilla

Vamos a probar el ejemplo [webcam](https://github.com/aiortc/aiortc/tree/main/examples/webcam) del módulo [aiortc](https://github.com/aiortc/aiortc), que proporciona una API HTTP y WebRTC para Python.

Usando el video `video.mp4`, prueba a verlo en el navegador. Para ello, lanza wl programa `webcam.py`:

```shell
python3 webcam.py --play-from video.mp4
```

A continuación, carga en Chrome la url http://0.0.0.0:8080/ (en Firefox, puede que no funcione). Pulsa el botón "Start", y deberías ver el video.

Realiza una captura de la sesión completa, y luego revísala en Wireshark.

Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
