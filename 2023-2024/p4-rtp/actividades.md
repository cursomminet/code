## RTP

Actividades:

* Repaso de Wireshark
* Análisis de trazas RTP
* Generación de trazas RTP con GStreamer

**Ejercicio:** [Práctica 4. RTP](ejercicios.md)

Referencias:

* [Wireshark](https://www.wireshark.org/).
* [RTP en Wireshark](https://wiki.wireshark.org/RTP).
* [Documentación sobre GStreamer](https://gstreamer.freedesktop.org/documentation/)
