## AsyncIO

Actividades:

* Entrada / salida asíncrona en Python: asyncio
* Descripción del ejercicio de entrega

**Ejercicio:** [Práctica 6. AsyncIO](ejercicios.md)

Referencias:

* [asyncio](https://docs.python.org/3/library/asyncio.html)
* [Async IO in Python: A Complete Walkthrough](https://realpython.com/async-io-python/)
* [sdp-transform](https://github.com/skymaze/sdp-transform)
