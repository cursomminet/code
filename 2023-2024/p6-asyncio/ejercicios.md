# Protocolos para la transmisión de audio y video en Internet
# Práctica 6. Envío asincrónico de datos UDP

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta un 0.7 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de uso de wireshark
* Nociones de UDP (de cursos anteriores)
* Nociones de SDP (de temas anteriores)

**Tiempo estimado:** 8 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2023-2024/p6-asyncio

**Fecha de entrega parte individual:** 10 de noviembre de 2023, 23:59 (hasta ejercicio 8, incluido)

**Fecha de entrega parte interoperación:** 13 de noviembre de 2023, 23:59 (ejercicio 9)

## Introducción

El módulo `asyncio` de Python proporciona una API asíncrona y no bloqueante para escribir código concurrente basado en eventos. `asyncio` usa corutinas y bucles de eventos para permitir la ejecución concurrente de tareas asíncronas, como la comunicación a través de sockets. En esta práctica utilizaremos `asyncio` para enviar y recibir datos UDP, llegando a intercambiar de esa manera documentos SDP.

## Objetivos de la práctica

* Conocer `asyncio` y cómo usarlo para enviar y recibir datos UDP.
* Repasar SDP, usándolo en paquetes UDP
* Utilizar el formato JSON

## Ejercicio 1. Creación de repositorio para la práctica

Con el navegador, dirígete al repositorio plantilla de esta práctica y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la EIF).

## Ejercicio 2. Prueba de asyncio

Prueba tu primer programa con `asyncio`:

```python
import asyncio

async def main():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')

asyncio.run(main())
```

En este programa tenemos una función asíncrona (`main`) que muestra las dos palabras de "¡Hola mundo!" en dos momentos, separados un segundo.

## Ejercicio 3. Empezando a entender asyncio

Para entender mejor cómo funciona asyncio, vamos a extender el ejercicio anterior. Mira el programa `hola.py` del repositorio plantilla. Ejecútalo, y estudialo. Observa cómo `asynio.gather` es una función asíncrona que espera a que terminen todas las funciones asíncronas que se le pasan como parámetro (en este caso, `hola` varias veces). Como la funión `hola` cede el procesador al quedarse esperando a `asyncio.sleep`, vemos cómo primero se muestran todos los "Hola", y después los "mundo".

Para entender mejor el código, es conveniente sabe que las funciones asíncronas devuelven un "futuro", que es un objeto cuya finalización se puede esperar con `await`. Y que cada vez que esperamos con `await` estamos bloquándonos, dejando que otras tareas que pudieran estar listas para ejecutar utilicen el procesador.

La línea `asyncio.run(main())` inicia un buble de ejecución, que básicamente comienza a ejecutar la función asíncrona `main` hasta que se bloquee en un `awair`, momento en que que se ejecutará cualquier otra tarea que esté lista para ejecutar, y así sucesivamente,

## Ejercicio 4. Contadores asíncronos

Utilizando los mecanismos vistos hasta el momento, construye un programa que lance varios contadores asíncronos. Cada contador será implementado por una función asíncrona que recibirá como parámetros un identificador de contador, y la cuenta de segundos que tiene que realizar (un entero):

```python
async def counter(id, time):
```

Cada contador mostrará un mensaje indicando que está empezando, un número con la cuenta de segundos (cada segundo) y un mensaje indicando que han terminado. Por ejemplo, si `id` es "A" y `time` es 4, el contador mostrará:

```shell
Counter A: starting...
Counter A: 1
Counter A: 2
Counter A: 3
Counter A: 4
Counter A: finishing...
```

Queremos que cuando funcionen varios contadores, cada uno vaya contando su tiempo en paralelo a los demás. Por ejemplo, si ejecutamos el contador de esta forma:

```python
async def main():
    await asyncio.gather(counter('A', 4), counter('B',2 ), counter('C',6))

asyncio.run(main())
```

Se mostrará en pantalla (las líneas con `[1seg]` no aparecen realmente, sino que representan el paso de un segundo; el orden entre A, B y C podría variar):

```python
Counter A: starting...
Counter B: starting...
Counter C: starting...
[1seg]
Counter A: 1
Counter B: 1
Counter C: 1
[1seg]
Counter A: 2
Counter B: 2
Counter B: finishing...
Counter C: 2
[1seg]
Counter A: 3
Counter C: 3
[1seg]
Counter A: 4
Counter A: finishing...
Counter C: 4
[1seg]
Counter C: 5
[1seg]
Counter C: 6
Counter C: finishing...
```

El objetivo final de este ejercicio es tener un fichero `counter.py` que tenga las líneas Python indicadas en este ejercicio, que produzca la salida anterior, y que incluya una implementación de la función asíncrona `counter`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 4"

## Ejercicio 5. Haciendo el bucle de ejecución explicito

La función `asyncio.run` es muy cómoda de usar en muchos casos, ya que se encarga de crear un bucle (loop) de ejecución de tareas, de esperar a que todas las tareas gestionadas por el buble terminen, y de cerrar el bucle, terminando.

En este ejercicio vamos a explorar otra opción menos transparente, qeu nos permite gestionar el bucle. Es la función `asyncio.get_event_loop`.

Tendrás que crear un fichero `hola_loop.py` que funcione como `hola.py`, pero qeu en lugar de la invocacoón a `asyncio.run` cree un bucle usando `asyncio.get_event_loop`, luego ejecutre ese blucle hasta que termine la función `main`, y cuando lo haga, lo cierre.

Puedes explorar cómo funciona `asyncio.get_event_loop` en la [sección sobre el bucle de eventos](https://realpython.com/async-io-python/#the-event-loop-and-asynciorun) del tutorial "Async IO in Python: A Complete Walkthrough"

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 5".

## Ejercicio 6. Cliente / servidor UDP de eco 

Estudia los ficheros `udp_echo_client.py` y `udp_echo_server.py`, que implementan un cliente y servidor de eco sobre UDP. Observa cómo se comportan cuando los ejecutas,y lee su código tratando de entenderlo.

Realiza una captura de su actividad (sólo los paquetes UDP que intercambian) y guárdala en el fichero `echo.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 6".

## Ejercicio 7. Cliente / servidor que intercambian SDP

Utiliza las facilidades que proporciona el paquete [sdp-transform](https://github.com/skymaze/sdp-transform) para enviar y recibir documentos SDP sobre paquetes UDP. Para ello, utiliza el esquema del programa cliente / servidor del ejercicio anterior para escribir dos programas, `sdp_offer.py` y `sdp_answer.py`. El primero enviará una oferta SDP, y el segundo la recibirá, y devolverá al primero su respuesta SDP (en ambos casos, encapsuladas en un paquete UDP).

Para poder utilizar este módulo, tendrás que instalarlo previamente en un entorno virtual. Recuerda que puedes crear el entorno virtual desde PyCharm o desde el terminar. En el segundo caso, tendrás que activarlo antes de ejecutar programas Python en él. Por ejemplo:

```shell
python3 -m venv venv
source venv/bin/activate
```

La segunda línea tendrás que ejecutarla cuando quieras activar el entorno virtual `venv` previamente creado.

En cuanto a los documentos SDP a intercambiar, el que enviará `sdp_offer.py' tendrá la siguiente información: 

```shell
{'version': 0,
  'origin': {'username': 'user',
    'sessionId': 434344,
    'sessionVersion': 0,
    'netType': 'IN',
    'ipVer': 4,
    'address': '127.0.0.1'},
  'name': 'Session',
  'timing': {'start': 0,
    'stop': 0},
  'connection': {'version': 4,
    'ip': '127.0.0.1'},
  'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                     {'payload': 96, 'codec': 'opus', 'rate': 48000}],
             'type': 'audio',
             'port': 54400,
             'protocol': 'RTP/SAVPF',
             'payloads': '0 96',
             'ptime': 20,
             'direction': 'sendrecv'},
            {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                    {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
             'type': 'video',
             'port': 55400,
             'protocol': 'RTP/SAVPF',
             'payloads': '97 98',
             'direction': 'sendrecv'}]}
```

El que enviará `sdp_answer.py` será igual, pero cambiando los dos campos `port` que aparecen, que pasarán a ser `34543`.

Realiza también una captura de los paquetes UDP intercambiados por ambos programas (y sólo esos paquetes), y guárdala en el fichero `sdp.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 7".

## Ejercicio 8. SDP encapsulado en JSON

Realiza dos programas, `sdpjson_offer.py` y `sdpjson_answer.py` que funcionen como los descritos en el ejercicio anterior, pero en lugar de enviar el documento SDP directamente encapsulado sobre UDP, lo enviarán como parte de un documento JSON. Este documento JSON tendrá dos claves (propiedades): `type` y `sdp `. Los valores (datos) de estas claves serán:

* "type": tipo de documento ("offer" o "answer")
* "sdp": documento sdp como cadena de texto (striong)

Los documentos SDP que intercambiarán serán los mismos que en el ejercicio anterior.

Para realizar estos programas, utiliza el módulo `json` de la biblioteca estándar de Python.

Realiza también una captura de los paquetes UDP intercambiados por ambos programas (y sólo esos paquetes), y guárdala en el fichero `sdpjson.pcap`.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 8".

## Ejercicio 9 (segundo periodo)

Para realizar esta práctica, tendrás que comenzar creando (si no lo has hecho antes) un nuevo grupo en el GitLab de la EIF, como se describe en el ejercico 11 de la práctica [p3-python](../p3-python/ejercicios.md).

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del repositorio plantilla de esta práctica uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la EIF de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `p6-asyncio-<alumno>`.

Crea un clon local de este nuevo repositorio, y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Llama a esta rama `nueva` (ver detalles en la práctica p3-python). 

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Crea en ella un nuevo programa, llamado `sdpjson_offer-<alumno>.py` que envíe una oferta con un documento SDP igual al original, pero cambiando el nombre de la sesión a `Segunda`. Prueba que al enviarlo, el programa de tu compañero te devuelve un documento SDP igual, pero con los puertos cambiados (como indica el enunciado del Ejercicio 9).

Cuando termines, crea un commit que incluya este nuevo fichero, y que tenga un comentario cuya primera línea sea "Ejercicio 10". Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes usar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).

## ¿Qué se valora de esta la práctica?

En está práctica valoraremos sobre todo que el código desarrollado realice lo indicado con la mayor exactitud posible. Además, la legibilidad del código, y en general el seguimiento estricto de las instrucciones serán tenidos en cuenta.
