# Protocolos para la transmisión de audio y video en Internet
# Práctica 7. XML y JSON

**Nota:** Esta no es una práctica entregable, aunque se recomienda su realización para afianzar los conocimientos sobre XML y JSON.

**Conocimientos previos necesarios:**

* Nociones de Python3 (prácticas previas)
* Nociones de orientación a objetos en Python3 (prácticas previas)
* Nociones de XML y SMIL (presentadas en clase de teoría)

**Tiempo estimado:** 6 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p3

**Práctica resuelta:** https://gitlab.etsit.urjc.es/ptavi/2022-2023-resueltas/ptavi-p3-resuelta

## Introducción

Python ofrece una serie de bibliotecas para manipular ficheros en XML (como SMIL) y JSON. En esta práctica, veremos cómo utilizar la biblioteca DOM (módulo `xml.dom.minidom`) y la biblioteca JSON (módulo `json`).

## Objetivos de la práctica

* Profundizar en el uso de SMIL, XML y JSON.
* Aprender a utilizar la biblioteca DOM para el manejo de XML, en particular con Python.
* Aprender a utilizar la biblioteca  JSON, en particular con Python.
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Inspecciona el fichero `chistes.py`, en este mismo directorio. Verás que el fichero consta de tres partes:

* Importación de del módulo `xml.dom.minidom`.

* La función `main`. Esta función utiliza la función `parse` de `xml.dom.minidom` para crear un DOM a partir del fichero XML `chistes.xml`, poniéndolo en la variable `document`. A continuación, obtiene la lista de elementos `chiste`, y para cada uno de ellos, obtiene el atributo `calificacion` y los elementos `pregunta` y `respuesta`. Finalmente, muestra en pantalla el contenido de estos elementos. Es interesante cómo en el caso de los elementos `pregunta` y `respuesta`, la función `getElementsByTagName` devuelve una lista de elementos (aunque sólo contenga un elemento). De ella se coge el primer elemento (el elemento 0), del que se obtiene el primer hijo (usando `first.Child`), que es también un elemento, del que se obtiene su valor como una cadena (usando `nodeValue`). Por último, de esta cadena se eleminan los espacios y fines de línea que pueda tener al principio y al final.

* La llamada a la función `main`, que solo se realiza cuando el script ha sido llamado como programa principal (en ese caso, al variable de sistema `__name__` tiene el valor `__main__`).

Ejecútalo, pruébalo en el depurador.

Incluye el programa `chistes.py`, tal y como está, en el repositorio de entrega (`git add`), y registra el cambio (`git commit`).

Una vez en el repositorio de entrega, ejecuta también sus tests:

```shell
python3 -m unittest tests/test_chistes.py
```

## Ejercicio 2

Crea un nuevo programa, `chistes2.py`, que escriba los chistes en orden inverso (primero el último que aparezca en el fichero XML), y que escriba primero las respuestas y luego las preguntas. Por lo demás, usa el mismo formato que produce `chistes.py`.

Asegúrate de que las instrucciones del programa principal están en una funcion (llamada `main`), pues así lo espera el test que este programa tendrá que pasar.

Puedes probar el programa ejecutando el test `test_chistes2.py`:

```shell
python3 -m unittest tests/test_chistes2.py
```

Incluye el programa `chistes2.py` en el repositorio de entrega (`git add`), y registra el cambio (`git commit`).

## Ejercicio 3

Crea un nuevo programa, `chistes3.py`, modificando el programa `chistes.py` para que:

* La función `main` admita un parámetro, `path`, que será el nombre del fichero con el documento XML de chistes.

* Escriba los chistes en orden según su calificación. Las calificaciones posibles son (en este orden, de mejor a peor): "buenisimo", "bueno", "regular", "malo", "malisimo".

* Tras cada chiste, escribirá una línea en blanco, para mejor separar los chistes unos de otros.

* Si el elemento raíz del fichero no se llama `humor`, la función `main` debe levantar la excepción Exception con el mensaje "Root element is not humor".

Por lo demás, el programa se comportará igual que `chistes.py`, y escribirá los chistes en el mismo formato.

Puedes probar el programa ejecutando el test `test_chistes3.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 4

Crea un nuevo progarma, `chistesoo.py`. Este programa producirá exactamente los mismos resultados que `chistes3.py`. Pero para ello, utilizará una clase, `Humor`. Esta clase tendrá las siguientes funciones:

* Una función de inicialización, `__init__`, que acetpará como parámetro un nombre de fichero (`path`) con un documento XML con chistes, en el mismo formato que `chistes.xml`.

* Una función `jokes`, qur devolverá la lista de chistes que haya en el fichero XML. Cada elemento de la lista tendrá información sobre un chiste, en forma de diccionario. Las claves de estos diccionarios serán `score`, `question` y `answer`, que tendrán la calificación, la pregunta y la respuesta del chiste. La lista está en orden de calificación, según se indicaba en el ejercicio anterior (desde `buenísimo` a `malísimo`).

La función `main` instanciará un objeto de esta clase para leer el fichero cuyo nombre le pasen como parámetro (igual que en el ejercico anterior), obtener el listado de chistes en él, y mostrarlo luego en pantalla.

Por lo demás, el programa se comportará igual que `chistes3.py`, y escribirá los chistes en el mismo formato.

Puedes probar el programa ejecutando el test `test_chistesoo.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 5

Crea un fichero `smil.py`, con dos clases llamadas `SMIL` y `Element`.

La clase `Element` tendrá las siguientes funciones:

* Una función de inicialización, `__init__`, que aceptará como parámetro un nombre de elemento, un diccionario de atributos, y una cadena con el contenido del elemento:
  * El nombre de elemento será un elemento de SMIL (por ejemplo 'layout` o 'img'), aunque no se comprobará que sea un elemento válido del estándar SMIL.
  * El diccionario de atributos tendrá todos los atributos del elemento. Sus claves serán los nombres de los atributos, y sus valores, los valores de estos atributos (de tipo string).

* Una función `name`, que devolverá el nombre del elemento.
* Una función `attrs`, que devolverá el diccionario de atributos.

La clase 'SMIL' tendrá dos funciones:

* Una función de inicialización, `__init__`, que acetpará como parámetro un nombre de fichero (`path`) con un documento SMIL, que habrá que analizar.

* Una función `elements', que devolverá una lista con todos los elementos del documento, cada uno de ellos de tipo `Elemento`, con sus datos. Esta lista tendrá los elementos en el orden en que aparecen en el documento.

Puedes probar el programa ejecutando el test `test_smil.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 6

Crea un programa `elements.py`, que tenga una función `main`, que se ejecutará cuando el programa sea llamado como módulo principal. Esta funciòn será lo único que se ejecute en ese caso (y si no se le llama como programa principal, no se ejecutará nada).

El programa aceptará un argumento de línea de comandos, que será el nombre el fichero que contenga un documento SMIL a analizar. La función main() conseguirá ese argumento, y escribirá en pantalla el nombre de todos los elementos que aparezcan en el documento, uno por líena, en el orden en que aparecen. Para ello, utilizará objetos de las clases `SMIL` y `Element` del fichero `smil.py`. Si no se encuentra un argumento de lína de comandos, y solamente uno, la función `main` terminará el programa escribiendo:

```
Usage: python3 elements.py <file> 
```

Para realizar pruebas, se puede utilizar el fichero `karaoke.smil`. Para este fichero, la salida del programa debe ser:

```
smil
head
layout
root-layout
region
region
region
body
par
img
img
audio
textstream
```

Puedes probar el programa ejecutando el test `test_elements.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 7

Crea un programa `tojson.py`, que tenga una función `main`, que se ejecutará cuando el programa sea llamado como módulo principal. Esta funciòn será lo único que se ejecute en ese caso (y si no se le llama como programa principal, no se ejecutará nada).

La función se comportará como la de `elements.py`, pero en lugar de escribir el listado de nombres de elementos, escribirá un documento JSON con un formato como el siguiente:

```json
[
  {
    "name": "smil",
    "attrs": {}
  },
  {
    "name": "head",
    "attrs": {}
  }, 
  ...
  {
    "name": "img",
    "attrs": {"src": "http://www.content-networking.com/smil/hello.jpg",
      "region": "a",
      "begin": "2s",
      "dur": "36s"
  },
  ...
]
```

Esto es, para cada elemento del documento SMIL, creará un diccionario, con las siguientes claves:

* `name`, que tendrá el nombre del elemento (de tipo `string`)
* `attrs`, que tendrá un diccionario con los atributos (con los nombres de los atributos como claves, y sus valores como varlores del diccionario), o un diccionario vacío si no hay ninguno.

Para hacerlo, se creará la clase `SMILJSON`, que heradará de la clase `SMIL` del fichero `smil.py`, y tendrá una nueva función `json` que devolverá un string con el documento JSON correspondiente al todo el fichero con que se inicializa. Esta clase se apoyará en la clase `ElementJSON`, que heredará de la clase `Element` del fichero `smil.py`, y tendrá una nueva función `dict` que devolverá un diccionario con los datos que habrá que utlizar para componer la parte del documento JSON corresondiente a ese elemento (y tendrá por tanto las claves `name`, `attrs` y `chlidren`).

Puedes probar el programa ejecutando el test `test_tojson.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 8

Crea otro programa `download.py`, también con su programa principal en una función `main`. Este programa aceptará un argumento en la línea de comandos, el nombre del archivo SMIL a procesar, y descargará en local el contenido multimedia remoto referenciado en ese archivo SMIL (atributo `src` de elmentos `img`, `audio` y `textstream`). De esta manera, si el atributo `src` de un elemento del archivo SMIL tiene como valor un elemento en remoto (o sea, una URL que empiece por `http://` o `https://`), se deberá descargar ese elemento a un fichero local.

Para descargar documentos correspondienets a una URL, utilizaremos la función [`urlretrieve`](https://docs.python.org/3/library/urllib.request.html#urllib.request.urlretrieve) de la biblioteca `urllib.request` de Python3.

Cada fichero que descargue lo descargará con el nombre `fichero-<n>.<ext>`, donde `fichero` será siempre el principio del nombre del fichero, `<n>` (empezando por 0) será el número de fichero que se descarga, en el orden en el que están en el fichero SMIL, y `<ext>` la extensión de la url que se descargó. Para identificar la extensión, se considerará suficiente partir la url usando el carácter punto como divisor (usando `split`) y quedarse con el último elmento de la lista resultante (quien quiera realizar una división más completa, puede usar `urlparse` y `splitext`).

Por ejemplo, si las urls son:

```
https://un.servidor.com/fichero.jpeg
http://otro.servidor.org/direct.orio/fich.2.mp3
```

los ficheros en que se crearán serán `fichero-0.jpeg` y `fichero-1.mp3`.

Puedes probar el programa ejecutando el test `test_download.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## ¿Cómo puedo probar esta práctica?

Para muchos de los apartados del programa, se proporciona un test. Puedes ejecutarlo para ver que el test pasa. Ten en cuenta que el hecho de que pase el test no quiere decir que la parte correspondiente esté completamente bien, aunque los tests están diseñados para probar los errores más habituales. Recuerda que si el test es `test_xxx.py`, para ejecutarlo podrás ejecutarlo en PyCharm, o desde la línea de comandos:

```shell
python3 -m unittest tests/test_xxx.py
```

Cuando tengas la práctica lista, puedes realizar una prueba general, incluyendo la comprobación del estilo de acuerdo a PEP8, que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```
