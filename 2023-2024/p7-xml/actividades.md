## XML y JSON

Actividades:

* Ejemplo de fichero XML: [chistes.xml](chistes.xml)
* Ejemplo de programa que extrae chistes de un fichero XML: [chistes.py](chistes.py)

**Ejercicio:** [Práctica 7. XML y JSON](ejercicios.md)

Referencias:

* [Módulo xml.dom.minidom](https://docs.python.org/3/library/xml.dom.minidom.html)
* [Reading (and writing) XML from Python](http://www.bioinf.org.uk/teaching/bbk/databases/dom/lecture/dom.pdf)