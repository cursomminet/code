## Proxy para acceso a videos vía WebRTC

Actividades:

* Repaso de WebRTC
* Descripción del ejercicio de entrega

**Ejercicio:** [Práctica 7. WebRTC-Proxy](ejercicios.md)

Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
