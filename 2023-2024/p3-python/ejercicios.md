# Protocolos para la transmisión de audio y video en Internet
# Práctica 3. Python avanzado

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.77 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python3 (las de las prácticas anteriores).
* Nociones de git (las de la práctica de git)

**Tiempo estimado:** 6 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2023-2024/p3-python

<!-- **Práctica resuelta:** https://gitlab.eif.urjc.es/ptavi/2023-2024-resueltas/p3-python-resuelta -->


**Fecha de entrega parte individual:** 10 de octubre de 2023, 23:59 (hasta ejercicio 10, incluido)

**Fecha de entrega parte interoperación:** 13 de octubre de 2023, 23:59 (ejercicio 11)

## Introducción

La programación orientada a objetos en un paradigma de programación muy
utilizado en la actualidad y que conviene conocer para la realización de las
prácticas de la asignatura. En esta práctica se pretende explorar los conceptos
más importantes de este paradigma, implementando funcionalidad en Python.

## Objetivos de la práctica

* Conocer y practicar la programación orientada a objetos (con conceptos como clase,
  objeto, herencia, instanciación).
* Usar varios modulos Python, importando funcionalidad creada en otros modulos.
* Conocer y seguir la guía de estilo de programación recomendada para Python (ver PEP8).
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Comprueba que puedes entrar en tu cuenta del [GitLab de la ETSIT](https://gitlab.eif.urjc.es).

## Ejercicio 2

Con el navegador, dirígete al repositorio plantilla de esta práctica (ver al comienzo de este enunciado)
y realiza un "fork" del mismo en tu cuenta, de manera que consigas tener una
copia del repositorio en tu cuenta. Clona en tu ordenador
local el repositorio que acabas de crear, para poder editar los archivos localmente.
Trabaja a partir de ahora en ese repositorio, sincronizando
los cambios (commits) que vayas efectuando según los ejercicios que se
detallan a continuación. Recuerda subir estos cambios a tu repositorio en
GitHub (normalmente, usando `git push`) para que queden reflejados allí.

## Ejercicio 3

Investiga el archivo `mysound.py`, en este repositorio.
Comprueba que en él se implementa una clase para almacenar muestras de sonido
como listas de números enteros. Lee el código de la clase, y trata de entenderla
lo mejor posible.

Si te resulta de ayuda, utiliza un asistente de IA para que te de explicaciones
sobre código que no entiendas.

Verás que hay también un programa `show_sound.py`. Trata de entenderlo, y ejecútalo
con distintos valores.

## Ejercicio 4

Mira en el directorio (carpeta) `tests`. Verás tests (pruebas) para los programas
anteriores. Ejecútalos:

```commandline
python3 -m unittest discover -s tests
```

Lee el código de los programas de prueba, y trata de entenderlos. Si te sirve de ayuda,
utiliza un asistenete de IA para que te los explique.

Aprovecha para leer sobre tests en [Testing Your Code](https://docs.python-guide.org/writing/tests/),
de "The Hitchhiker's Guide to Python" (sobre todo la parte sobre `unittest`) y
la documentación de [`unittest`](https://docs.python.org/3/library/unittest.html).

Añade un nuevo fichero a la carpeta `tests`, llamado `tests/test_mio.py`.
Incluye en él al menos un test que pruebe cualquier aspecto del programa que quieras.
Comprueba que el test funciona bien, que su resultado es OK.
Haz un commit que incluya a este fichero, y que tenga un comentario cuya
primera línea sea "Ejercicio 4". Sube (haz "push") de ese commit a tu repositorio
de entrega.

## Ejercicio 5

Crea un nuevo fichero `mysoundsin.py`. En él, crea una clase llamada `SoundSin`,
extendiendo (heredando de) `Sound`, cuyo método `__init__` acepte los parámetros
que acepta `Sound.__init__` y los que acepta `Sound.sin`,
y que inicialice los objetos `SoundSin` como los objetos
`Sound` si se llamase siempre a `Sound.sin` justo después de crearlos. En otras
palabras, su código de inicialización creará un buffer, igual que `Sound`, pero
al terminar la creación del objeto tendremos ya en él una seña sinusoidal.

El método `Sound.Sin.__init__` tendrá por tanto la firma (signatura) siguiente:

```python
def __init__(self, duration, frequency, amplitude)
```

La clase no tendrá más funciones propias (pero deberán seguir funcionando la
funciones heredadas de la clase `Sound`).

Haz un commit que incluya a este nuevo fichero, y que tenga un comentario cuya
primera línea sea "Ejercicio 5". Sube (haz "push") de ese commit a tu repositorio
de entrega.


## Ejercicio 6

Crea un nuevo fichero, `tests/test_soundsin.py` que realice alguna prueba
sobre la clase `SoundSin`. Al menos, que sean tres métodos (funciones) de test
distintos, que prueben distintos aspectos de la creación de objetos `SoundSin`,
pero también que prueben alguna funcionalidad heredada de `Sound`.
Prueba que todas las pruebas resultan en OK.

Haz un commit que incluya a este nuevo fichero, y que tenga un comentario cuya
primera línea sea "Ejercicio 6". Sube (haz "push") de ese commit a tu repositorio
de entrega.


## Ejercicio 7

Crea un nuevo fichero, `soundops.py` que incluya una función, `soundadd` que sirva para
sumar dos sonidos (objetos de la jerarquía  `Sound`). Para sumarlos, simplemente
suma el valor de la muestra 0 de los dos sonidos a sumar, resultando en la muestra 0
del sonido resultante, luego suma el valor de la muestra 1 de los dos sonidos a sumar,
resultando en la muestra 1 del sonido resultante, y así sucesivamente hata que terminen
las muestras de uno de lo sonidos a sumar. A partir de ahí, las muestras del sonido
resultante serán las del sonido a sumar que aún no terminó, hasta que termine.

Por ejemplo,, si tenemos dos objetos `s1` y `s2` de la clase `Sound`, el primero con 20
muestras y el segundo con 30 muestras, al sumarlos, el sonido resultante tendrá 30
muestras, siendo las 20 primeras la suma de las primeras 20 muestras de `s1` y `s2`,
y las 10 últimas las 10 últimas de `s2`.

La función `soundadd` tendrá la siguiente firma (signatura):

```python
def soundadd(s1: Sound, s2: Sound) -> Sound
```

Haz un commit que incluya a este nuevo fichero, y que tenga un comentario cuya
primera línea sea "Ejercicio 7". Sube (haz "push") de ese commit a tu repositorio
de entrega.


## Ejercicio 8

Crea un nuevo fichero, `tests/test_soundadd.py` que realice alguna prueba
sobre la funcion `soundadd` que has creado en el ejercicio anterior.
Al menos, que sean tres métodos (funciones) de test
distintos, que prueben distintos casos de suma de objetos `Sound` (por ejemplo,
que los objetos a sumar tengan distintas longitudes). 
Prueba que todas las pruebas resultan en OK.

Haz un commit que incluya a este nuevo fichero, y que tenga un comentario cuya
primera línea sea "Ejercicio 8". Sube (haz "push") de ese commit a tu repositorio
de entrega.

## Ejercicio 9

Aunque el intérprete de Python admite ciertas libertades a la hora de
programar, los programadores de Python con la finalidad de mejorar
principalmente la legibilidad del código recomiendan seguir una guía
de estilo. Esta guía de estilo se encuentra en el [Python Enhancement
Proposal 8 (PEP 8)](https://www.python.org/dev/peps/pep-0008/), y contiene instrucciones sobre cómo situar los
espacios en blanco, cómo nombrar las variables, etc. Puedes leer también una
[explicación en español](https://ellibrodepython.com/python-pep8).

Hay programas para comprobar que un programa sigue correctamente PEP8, que además
te explican qué errores tienes si encuentran algunos. Para Python3 recomendamos
utilizar [pycodestyle](https://pycodestyle.pycqa.org)
(en Ubuntu, disponible si instalas el paquete `pycodestyle`), que puedes
aplicar a tus programas hasta que no detecte ningún error.
También puedes usar la [funcionalidad que proprociona PyCharm](https://www.jetbrains.com/help/pycharm/tutorial-code-quality-assistance-tips-and-tricks.html),
que tambien detecta los errores PEP8 si está adecuadamente configurado,
y te puede ayudar a reformatear el código para resolver esos errores.

Haz un commit que incluya los ficheros ya modificados de forma que `pycodestyle`
no detecte errores, y que tenga un comentario cuya
primera línea sea "Ejercicio 9". Sube (haz "push") de ese commit a tu repositorio
de entrega.

Cuando hayas  terminado todo, y estés listo para la entrega, comprueba que
tu repositorio no es privado (puedes cambiarlo en los últimos momentos antes del
momento de recogida de la práctica, si así lo prefieres).

# Ejercicio 10

Asegúrate, comprobando en el GitLab de la ETSIT, que todos tus cambios están en tu repositorio de entrega de la práctica.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio de entrega, creado de la forma que 
hemos indicado (como fork del repositorio plantilla que os proporcionamos).
Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:
* Se valorará que haya realizado al menos haya ocho commits sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorará que los programas, clases y funciones se llamen exactamente según se especifica, que acepten los parámetros y tengan las firmas que se indica, y en general, que sigan con detalle las instrucciones de este enunciado.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.

## Ejercicio 11 (segundo periodo)

Para realizar esta práctica, tendrás que comenzar creando un nuevo grupo en el GitLab de la EIF. Los grupos son una especie de usuarios virtuales, que permiten que varios usuarios colaboren en un proyecto común. Por ejemplo, [ptavi](https://gitlab.etsit.urjc.es/ptavi) es uno de estos grupos. En este caso, los vamos a utilizar para poder hacer un segundo fork con origen en el repositorio plantilla de la práctica, como verás más adelante.

Para hacer un grupo, puedes seguir las [instrucciones en el manual de GitLab](https://docs.gitlab.com/ee/user/group/#create-a-group). Como nombre del grupo utiliza `forks-<user>`, siendo `<user>` el nombre de usuario que estás usando en el GitLab de la EIF. Así, si tu nombre fuera `jgb`, tu grupo sería `forks-jgb`.

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del repositorio plantilla de esta práctica uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la EIF de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `p3-python-<alumno>`.

Crea un clon local de este nuevo repositorio (usando `git clone`), y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Llama a esta rama `nueva`. Esto lo podrás hacer con el siguiente comando (que crea la rama `nueva`  y te deja en ella:

```shell
git checkout -b nueva
```

Podrás ver en qué rama estás con el comando `git branch`. Si te hace falta, podrás cambiar de nuevo a la rama principal con `git checkout master`, y volver a la rama `nueva` con `git checkout nueva` (ojo: si has hecho cambios, tendrás que hacer primero un commit con ellos, para poder cambiar de rama sin perder datos).

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Crea en ella un nuevo test,
llamado `tests/test_<user>.py` (donde `<user>` sea tu identificador, según se indica más arriba),
que tenga al menos un método (función) de prueba de cualquier aspecto
del código de tu compañero. 

Cuando termines, crea un commit (usando `git add` y `git commit`, o desde PyCharm) que incluya estos cambios, y que tenga un comentario cuya
primera línea sea "Ejercicio 11". Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes usar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).
