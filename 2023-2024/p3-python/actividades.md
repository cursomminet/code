## Python avanzado

Actividades:

* Repaso de programación orientada a objetos
* Repaso sobre las excepciones en Python
* Cómo ejecutar tests de Python
* Cómo comprobar el estilo PEP8 de un fichero Python

**Ejercicio:** [Práctica 3. Python avanzado](ejercicios.md)

Referencias:

* [Capítulo "Classes & Iterators" de "Dive into Python3"](https://diveintopython3.net/iterators.html).
* [Apartado "Exceptions"
en "Dive into Python3"](https://diveintopython3.net/your-first-python-program.html#exceptions).
* [Testing Your Code](https://docs.python-guide.org/writing/tests/),
de "The Hitchhiker's Guide to Python" (sobre todo la parte sobre `unittest`)
* [Documentación de unittest](https://docs.python.org/3/library/unittest.html).
