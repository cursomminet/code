#!/usr/bin/env python3

""" Servidor WebRTC sencillo

Crea una peer connection (conexión WebRTC), usa como señalización
(para recibir ofertas y enviar respuestas) al usuario, que copia y pega
y cuando el canal de datos está establecido, recibe mensajes (`ping`) y
contesta con `pong`.
"""

import asyncio
import json
import sys

from aiortc import RTCPeerConnection, RTCSessionDescription


async def input_reader(bucle):
    """Crea un lector de la entrada estandar

    Este lector se puede usar dentro del bucle de eventos, donde
    debido a la asincronía, el `input`normal no funciona."""

    lector = asyncio.StreamReader(loop=bucle)
    read_transport, _ = await bucle.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(lector), sys.stdin
    )
    return lector


async def main():
    """Código princial"""

    # Crea una peer connection (conexión WebRTC)
    pc = RTCPeerConnection()

    @pc.on("datachannel")
    def on_datachannel(canal):
        print(f"Creado el canal {canal.label} (por el cliente).")

        @canal.on("message")
        def on_message(message):
            """Código a ejecutar cuando se recibe un mensaje"""

            print(f"Recibido por canal {canal.label}: {message}")
            # Una vez recibido un mensaje, respondemos `pong`
            message = "pong"
            print(f"Enviado por canal {canal.label}: {message}")
            canal.send(message)

    # Crea un lector de la entrada estandard, para poder leer lo que
    # escriba el usuario
    bucle = asyncio.get_event_loop()
    reader = await input_reader(bucle)
    while True:
        # Pide al usuario que pegue la respuesta del servidor
        print("-- Pega la oferta del cliente (recepción de señalización) --")
        # Lee la respuesta del servidor, y conviértela en un diccionario
        mensaje_str = await reader.readline()
        mensaje = json.loads(mensaje_str)
        if mensaje["type"] == "offer":
            # Si lo que ha pegado el usuario es una oferta, crea
            # un objeto RTCSessionDescription con esos datos
            respuesta = RTCSessionDescription(mensaje["sdp"], mensaje["type"])
            # Configura la parte remota de la peer connection (conexión WebRTC)
            await pc.setRemoteDescription(respuesta)
            # Crea una configuración para la parte local de la peer connection
            # (conexión WebRTC)
            config = await pc.createAnswer()
            # Configura la parte local de la peer connection (conexión WebRTC)
            await pc.setLocalDescription(config)
            # Obtén un documento SDP con los datos de la parte local
            # de la conexión WebRTC
            sdp = pc.localDescription.sdp
            # Crea un diccionario que sirva de oferta, y conviértelo en un
            # documento (string) en formato JSON
            respuesta = {"type": "answer", "sdp": sdp}
            respuesta_str = json.dumps(respuesta)

            # Pide al usuario que copie la oferta al cliente
            print("-- Copia esta respuesta en el cliente (envío de señalización) --",
                  flush=True)
            print(respuesta_str)

        else:
            # Si lo que ha pegado el usuario no es una oferta, ignóralo
            print("El mensaje de señalización no era una oferta, ignorándolo.")


if __name__ == "__main__":
    asyncio.run(main())
