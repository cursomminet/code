#!/usr/bin/env python3

""" Cliente WebRTC sencillo

Crea una peer connection (conexión WebRTC), usa como señalización
(para enviar la oferta y la respuesta) al usuario, que copia y pega
y cuando el canal de datos está establecido, envía un `ping` y se
queda esperando un `pong`.
"""

import asyncio
import json
import sys

from aiortc import RTCPeerConnection, RTCSessionDescription


async def input_reader(bucle):
    """Crea un lector de la entrada estandar

    Este lector se puede usar dentro del bucle de eventos, donde
    debido a la asincronía, el `input`normal no funciona."""

    lector = asyncio.StreamReader(loop=bucle)
    read_transport, _ = await bucle.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(lector), sys.stdin
    )
    return lector


async def main():
    """Código princial"""

    # Crea un futuro para indicar cuando hemos terminado
    bucle = asyncio.get_event_loop()
    terminado = bucle.create_future()

    # Crea una peer connection (conexión WebRTC)
    pc = RTCPeerConnection()
    # Crea un canal de datos
    canal = pc.createDataChannel("chat")
    print(f"Creado el canal {canal.label}.")

    @canal.on("open")
    def on_open():
        """Código a ejecutar cuando se abre el canal"""

        message = "ping"
        print(f"Enviado por canal {canal.label}: {message}")
        canal.send(message)

    @canal.on("message")
    def on_message(message):
        """Código a ejecutar cuando se recibe un mensaje"""

        print(f"Recibido en canal {canal.label}: {message}")
        # Cuando hemos recibido un mensaje, terminamos
        # (damos valor a la promesa terminado)
        terminado.set_result(True)

    # Crea una configuración para la parte local de la peer connection
    # (conexión WebRTC)
    config = await pc.createOffer()
    # Configura la parte local de la peer connection (conexión WebRTC)
    await pc.setLocalDescription(config)
    # Obtén un documento SDP con los datos de la parte local
    # de la conexión WebRTC
    sdp = pc.localDescription.sdp
    # Crea un diccionario que sirva de oferta, y conviértelo en un
    # documento (string) en formato JSON
    oferta = {"type": "offer", "sdp": sdp}
    oferta_str = json.dumps(oferta)

    # Pide al usuario que copie la oferta al servidor
    print("-- Copia esta oferta en el servidor (envío de señalización) --", flush=True)
    print(oferta_str)

    # Crea un lector de la entrada estandard, para poder leer lo que
    # escriba el usuario
    reader = await input_reader(bucle)
    # Pide al usuario que pegue la respuesta del servidor
    print("-- Pega la respuesta del servidor (recepción de señalización) --")
    # Lee la respuesta del servidor, y conviértela en un diccionario
    mensaje_str = await reader.readline()
    mensaje = json.loads(mensaje_str)
    if mensaje["type"] == "answer":
        # Si lo que ha pegado el usuario es una respuesta, crea
        # un objeto RTCSessionDescription con esos datos
        respuesta = RTCSessionDescription(mensaje["sdp"], mensaje["type"])
        # Configura la parte remota de la peer connection (conexión WebRTC)
        await pc.setRemoteDescription(respuesta)
    else:
        # Si lo que ha pegado el usuario no es una respuesta, termina
        exit("El mensaje de señalización no era una respuesta.")
    # No terminamos la corrutina hasta que se haya recibido el mensaje pong
    await terminado


if __name__ == "__main__":
    asyncio.run(main())
