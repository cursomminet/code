# Protocolos para la transmisión de audio y video en Internet
# Ejercicios recomendados. Introducción a WebRTC

**Objetivos:** Comenzar a trabajar con WebRTC

**Conocimientos teóricos previos:** Conocimientos teóricos básicos de WebRTC.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 1 hora.

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Ejercicio 1

Vamos a probar el ejemplo [webcam](https://github.com/aiortc/aiortc/tree/main/examples/webcam) del módulo [aiortc](https://github.com/aiortc/aiortc), que proporciona una API HTTP y WebRTC para Python.

Usando el video `video.mp4`, prueba a verlo en el navegador. Para ello, lanza wl programa `webcam.py`:

```shell
python3 webcam.py --play-from video.mp4
```

A continuación, carga en Chrome la url http://127.0.0.1:8080/ (en Firefox, puede que no funcione). Pulsa el botón "Start", y deberías ver el video.

Realiza una captura de la sesión completa, y luego revísala en Wireshark.

Para poder probar `webcam.py` tendrás que crear un entorno virtual Python, e instalar en él los paquetes [aiortc](https://github.com/aiortc/aiortc) y [aiohttp](https://github.com/aio-libs/aiohttp).


Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
