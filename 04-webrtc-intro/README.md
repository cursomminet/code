## Introducción a WebRTC

Actividades:

* Prueba de una aplicación sencilla WebRTC
* Prueba de un módulo Python que implementa WebRTC
* Uso de WebRTC para transmitir datos entre pares
* Descripción del ejercicio de entrega

## Ejercicios

[Ejercicios recomendados](ejercicios-recomendados.md)

[Ejercicios de entrega evaluable (minipráctica)](ejercicios.md)

Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
