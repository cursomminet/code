# Protocolos para la transmisión de audio y video en Internet
# Ejercicios recomendados. Proyecto final

**Objetivos:** Entender la interacción entre un navegador, un programa que sirve videos mediante WebRTC, y servidor web (en el mismo programa), que hace las funciones de interfaz de usuario y señalizador.

**Conocimientos teóricos previos:** Conocimientos teóricos básicos de WebRTC, prácticas anteriores de WebRTC.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 1 hora.

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Ejercicio 1

Vamos a probar el programa [front.py](front.py). Este programa tiene dos partes:

* Un servidor web (HTTP), construido usando `aiohttp`. Este servidor atiende a tres recursos (rutas):
    * `GET /`: Devuelve la página HTML [index.html](index.html). Esta página incluye código JavaScript que permite a los navegadores ver el video que se reciba mediante WebRTC. El programa JavaScript se encuentra en el archivo [viewer.js](viewer.js).
    * `GET /viewer.js`: Devuelve el programa JavaScript [viewer.js](viewer.js). Este programa atenderá a que el usuario pulse el botón "Play", y cuando lo haga, configurará la parte local de una conexión WebRTC, enviará un `POST /offer` con la oferta correspondiete al servidor HTTP y quedará esperando a que este le responda con una respuesta WebRTC, que utlizará para configurar la parte remota de su conexión, y comenzar a recibir el video.
    * `POST /offer`: Envía una oferta WebRTC, con el documento SDP necesario para establecer una conexión WebRTC con el navegador.
* Un nodo (peer) WebRTC, que usará la oferta recibida del navegador, mediante el `POST /offer` para configurar la parte remota de una conexión WebRTC, luego configurará su parte local indicando que va a enviar el video `video.webm`, y enviará una respuesta WebRTC al navegador (como respuesta al `POST`), que el navegador utilizará para configurar su parte remota de la conexión WebRTC, y comenzar a recibir el video.

Para ejecutar el programa, asegúrate de que tienes instalados los módulos `aiortc` y `aiohttp`. Lanza el servidor web con el comando:

```commandline
python3 front.py
```

En el navegador, visita la dirección `http://localhost:8080/`. Pulsa el botón "Play" y espera a que el navegador te pida el video. En Firefox, pulsa también "Use STUN Server". En Chrome funciona pulsándolo o sin pulsar (pero si lo pulsas normalmente tarda como medio minuto en cargar el video).

Revisa el código fuente de `front.py`, y trata de entender todo su código. Revisa el programa `viewer.js` y trata de entenderlo. aunque no es tan importante para los objetivos de este proyecto final. Trata de entender también el fichero `index.html`.

## Ejercicio 2

Haz lo mismo para el programa [front_multi.py](front_multi.py), que permite elegir en el navegador qué video quieres ver entre las sesiones que ofrece. Busca las diferencias con [front.py](front.py). Y también las diferencias de [viewer.js](viewer.js) e [index.html](index.html) con [viewer_multi.js](viewer_multi.js) e [index_multi.html](index_multi.html). Trata de comprender estas diferencias, lo que te será muy útil para el proyecto final.