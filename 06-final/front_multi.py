import argparse
import asyncio
import json
import os
import sys

from aiohttp import web
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer

sessions = {
    "Sesión 1": "video.webm",
    "Sesión 2": "video2.webm"
}

async def offer(request):
    """ Corrutina que atiende las peticiones POST a /offer

    Se encarga de tramitar la oferta WebRTC recibida del navegador,
    crear una peer connection (conexión WebRTC) que incluye el
    canal de video correspondiente, y responder con la respuesta
    (answer) WebRTC correspondiente.
    """

    # Obtiene la oferta WebRTC recibida del navegador
    params = await request.json()
    print("Oferta WebRTC recibida del navegador:", params)
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
    # Según la sesión, elige el video a enviar
    session = params["session"]
    if session in sessions:
        video_file = sessions[session]
    else:
        print("Sesión no existente:", session)
        return web.Response(content_type="application/json",
                            text={"error": "Sesión no existente"})

    # Configura una conexión WebRTC (peer connection) y añádela al
    # conjunto de conexiones
    pc = RTCPeerConnection()
    pcs.add(pc)

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        """Corrutina que se ejecuta cuando cambia el estado de la
        conexión WebRTC (lo muestra, y cierra la conexión si falla.
        """

        print("Connection state is %s" % pc.connectionState)
        if pc.connectionState == "failed":
            await pc.close()
            pcs.discard(pc)

    # Crea un reproductor (player) del video a enviar
    player = MediaPlayer(video_file, decode=False)
    audio, video = player.audio, player.video

    # Si el video tiene audio y/o video añade los canales (tracks)
    # a la conexión
    if audio:
        pc.addTrack(audio)
    if video:
        pc.addTrack(video)

    # Configura la parte remota de la conexión con los datos
    # recibidos en la oferta
    await pc.setRemoteDescription(offer)
    # Configura la parte local de la conexion, y crea la respuesta
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    answer_dict = {"sdp": pc.localDescription.sdp,
                   "type": pc.localDescription.type}
    answer_json = json.dumps(answer_dict)
    print("Respuesta WebRTC enviada al navegador:", answer_json)
    # Lo que devuelve esta corrutina es lo que se enviará al navegador
    return web.Response(content_type="application/json",
                        text=answer_json)


# Conjunto para registrar todas las conexiones WebRTC (peer connections)
pcs = set()
# Directorio raiz (aquel en el que está este fichero)
ROOT = os.path.dirname(__file__)

async def on_shutdown(app):
    """Corrutina que cierra conexiones WebRTC
    cuando el servidor se apaga
    """

    # Corrutinas que van a cerrar las conexiones (peer connections)
    coros = []
    for pc in pcs:
        coro = pc.close()
        coros.append(coro)
    # Espera a que termine el cierre de todas las conexiones
    await asyncio.gather(*coros)
    pcs.clear()

async def index(request):
    """Corrutina que sirve la página principal (index.html)
    si se recibe una petición GET a /
    """

    content = open(os.path.join(ROOT, "index_multi.html"), "r").read()
    return web.Response(content_type="text/html", text=content)

async def viewer_js(request):
    """Corrutina que sirve viewer.js
    si se recibe una petición GET a /viewer.js
    """

    content = open(os.path.join(ROOT, "viewer_multi.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)

def main(port):
    # Configura y lanza el servidor web
    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/viewer_multi.js", viewer_js)
    app.router.add_post("/offer", offer)
    web.run_app(app, host="0.0.0.0", port=port)


if __name__ == "__main__":

    if len(sys.argv) == 2:
        port = sys.argv[1]
    else:
        port = 8080

    main(port)
