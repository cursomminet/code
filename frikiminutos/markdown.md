## Markdown

Markdown es un lenguaje de marcado que se utiliza mucho en documentación
de programas de ordenador, en wikis, y en otros ámbitos. Nosotros lo
estamos utilizando en todos los documentos de texto de esta asignatura.
Está diseñado para ser sencillo de leer, incluso si se ve "en crudo"
(sin transformar en HTML, que es como se ve "más bonito").

Más información:

* [Markdown (original)](https://daringfireball.net/projects/markdown/)
* [Markdown (GitHub)](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
* [Markdown (pandoc)](https://pandoc.org/MANUAL.html#pandocs-markdown)

Herramientas:

* [pandoc](https://pandoc.org/): genera muchos formatos a partir de Markdown (por ejemplo HTML y PDF) y muchos otros formatos
* [Soporte para Markdown en PyCharm](https://www.jetbrains.com/help/pycharm/markdown.html)
