## Transcripción de voz (de voz a texto)

Transcribe a texto un fichero de audio con voz (en inglés, español, u otros idiomas): [transcribe.py](transcribe.py)

Desde línea de comandos:

```commandline
whisper recording.wav --language Spanish --model medium
```

* Dependencias: [whisper](https://github.com/openai/whisper)

```commandline
pip install openai-whisper
```
