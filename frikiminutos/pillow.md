## Pillow, biblioteca para trabajar con imágenes

* [Página del proyecto](https://python-pillow.github.io/)
* [Tutorial](https://pillow.readthedocs.io/en/latest/handbook/tutorial.html)
* [Documentación](https://pillow.readthedocs.io/en/latest/)
* [Repositorio de desarrollo](https://github.com/python-pillow/Pillow)