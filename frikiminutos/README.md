# Frikiminutos (PTAVI)

* 2024-09-20. [Sitios para practicar programación](practicas.md)
* 2024-09-27. [Servidor web Python de una línea](python_http.md)
* 2024-10-04. [GStreamer](gstreamer.md)
* 2024-10-11. [Markdown](markdown.md)
* 2024-10-18. [GitLab con ssh](gitlab_ssh.md)
* 2024-10-25. [Descarga de audio de YouTube](pytube/README.md)
* 2024-11-08. [FFmpeg](ffmpeg.md)
* 2024-11-11. [OpenCV](opencv/README.md)
* 2024-11-14. [De habla a texto](habla-texto/README.md)
* 2023-11-20. [Shotcut, editor de video](shotcut.md)
* 2023-12-09. [Pillow](pillow.md)

