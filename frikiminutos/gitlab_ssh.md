## GitLab con ssh

GitLab (y git) puede usarse con ssh en lugar de HTTPS, y eso tiene algunas ventajas.
Para ello, se usa una clave SSH para conectarse a los repositorios de git.
Para poder hacerlo, hay que generar primero un par público / privado de claves ssh,
hay que registrar la clave pública en GitLab, y finalmente hay que usar la
url ssh en lugar de la url https al clonar, y en general, acceder, a los repositorios git.

Referencias:

* [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html)
* [How To Use SSH to Connect to a Remote Server](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server)
* [A beginner’s guide to SSH for remote connection on Linux](https://opensource.com/article/20/9/ssh)