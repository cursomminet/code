#!/usr/bin/env python

import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('../background/cafe.jpg', cv.IMREAD_GRAYSCALE)
assert img is not None, "File could not be read"
edges = cv.Canny(img,10,150)
plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
plt.show()