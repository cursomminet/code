## OpenCV

OpenCV es una biblioteca de software libre para trabajar con visión por computador.

Ejemplo: [edges.py](edges.py)

Instalación (para el ejemplo):

```commandline
pip install opencv-python matplotlib
```
Referencias:

* [Repositorio GitHub](https://github.com/opencv/opencv)
* [Documentación](https://docs.opencv.org/master/)