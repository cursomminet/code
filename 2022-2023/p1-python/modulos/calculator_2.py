#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Very simple calculator. This one checks for simple errors"""

import calc

def get_float(text):
    """Reads a string, tries to convert it to float, repeats if it fails"""
    while True:
        try:
            number = float(input(text))
            return number
        except ValueError:
            print("This was not a number")
            pass

ops = {
    '+': calc.add,
    '-': calc.sub,
    '*': calc.mul,
    '/': calc.div
}

def get_sign(text):
    """Reads a string, checks if it is an op, repeats if it fails"""
    while True:
        op = input(text)
        if op in ops:
            return op

if __name__ == "__main__":
    follow_on = 'C'
    while follow_on == 'C':
        first = get_float("Please enter an integer/float (first number): ")
        second = get_float("Please enter an integer/float (second number): ")
        op = get_sign("Please enter a sign (+,-,*,/): ")
        result = ops[op](first,second)
        print(f"The result of {first} {op} {second} is {result}")
        follow_on = input("Give me a C if you want to continue (anything else will finish): ")