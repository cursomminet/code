#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Module for simple calculator operations"""

def add(op1, op2):
    """Adds two integer/floats"""
    return op1 + op2

def sub(op1, op2):
    """Substracts two integer/floats"""
    return op1 - op2

def mul(op1, op2):
    """Multiplies two integer/floats"""
    return op1 * op2

def div(op1, op2):
    """Divides two integer/floats"""
    return op1 / op2

if __name__ == "__main__":
    num1: int = 5
    num2: int = 3
    print(f"Adding {num1} and {num2}: {add(num1, num2)}")
    print(f"Substracting {num1} minus {num2}: {sub(num1, num2)}")
    print(f"Multiplying {num1} by {num2}: {mul(num1, num2)}")
    print(f"Dividing {num1} by {num2}: {div(num1, num2)}")
