#!/usr/bin/python3
# -*- coding: utf-8 -*-

print("Crea y escribe en el fichero")
fich=open('prueba.txt','w')
fich.write("lunes\n")
fich.close()

print("Escribe algo más en el fichero")
fich=open('prueba.txt','a')
fich.write("martes\n")
fich.close()

print("Lee el fichero y muéstralo")
fich=open('prueba.txt','r')
lineas=fich.readlines()
fich.close()
for linea in lineas:
    print(linea, end='')

print("Vuelve a leerlo y mostrarlo")
with open('prueba.txt', 'r') as fich:
    lineas = fich.readlines()
    for linea in lineas:
        print(linea, end='')
