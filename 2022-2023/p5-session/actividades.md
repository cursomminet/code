## Sesión SIP

Actividades:

* Frikiminutos Python: [Arte ASCII: imágenes y videos](../python-snippets/README.md#asciiart2).
* Frikiminutos Python: [Grabación de audio](../python-snippets/README.md#soundrecord).
* Explicación de la solución a la práctica 4.
* Explicación del enunciado de la práctica 5.
* Algunos detalles de la traza a analizar.
* Repaso de uso de wireshark.

**Ejercicio:** [Práctica 5. Sesión SIP](ejercicios.md)
