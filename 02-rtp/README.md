## RTP y análisis de paquetes

Actividades:

* Presentación: Wireshark
* Práctica: Análisis de paquetes RTP con Wireshark
* Práctica: Generación de trazas RTP con GStreamer

Referencias:

* [Wireshark](https://www.wireshark.org/).
* [Wireshark documentation](https://www.wireshark.org/docs/)
* [RTP en Wireshark](https://wiki.wireshark.org/RTP)
* [GStreamer](https://gstreamer.freedesktop.org/)
* [Documentación sobre GStreamer](https://gstreamer.freedesktop.org/documentation/)
* [Instalación de GStreamer](https://gstreamer.freedesktop.org/documentation/installing/)

## Ejercicios

[Ejercicios recomendados](ejercicios-recomendados.md)

[Ejercicios de entrega evaluable (minipráctica)](ejercicios.md)

