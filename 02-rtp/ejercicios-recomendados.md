# Protocolos para la transmisión de audio y video en Internet
# Ejercicios recomendados. Análisis de RTP


**Objetivos:** Repasar el análisis de protocolos con Wireshark. Explorar la generación de paquetes con Gstreamer. Explorar el protocolo RTP y los conceptos relacionados con la transmisión de datos con restricciones de tiempo real.

**Conocimientos teóricos previos:** Conocimientos básicos
de Wireshark.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 1 hora.

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Introducción

Para generar paquetes RTP podemos utilizar las herramientas que vienen con Gstreamer. Podemos capturar y analizar estos paquetes con Wireshark, y de esta forma explorar el análsis de streams RTP.

## Ejercicio 1. Captura de una traza RTP

Vamos a realizar la captura de una traza RTP. Para ello, vamos a usar [GStreamer](https://gstreamer.freedesktop.org/), un sistema de tratamiento de sonido bastante completo, que entre otras cosas permite transmitir con paquetes RTP un fichero de audio.

Para generar un flujo RTP, vamos a utilizar el siguiente comando:

```shell
gst-launch-1.0  -v filesrc location = recording.wav ! wavparse ! audioconvert ! rtpL16pay ! udpsink host=127.0.0.1 port=1200
```

Antes de ejecutar el comando, prepara una captura (con Wireshark) en el dispositivo lo (loopback), de forma que tengas en ella todos los paquetes que se transmitan. Justo antes de hacerlo, ejecuta el comando `date` para obtener la fecha y hora de tu máquina. Guarda tu captura en el fichero `captura.pcap`.

Obtén lo siguientes resultados:

* Salida del comando `date`:
* Número total de paquetes en la captura:
* Duración total de la captura (en segundos):

## Ejercicio 2. Filtrado de la captura

Carga en Wireshark el fichero `captura.pcap`, filtra los paquetes RTP, y guárdalos en un nuevo fichero de captura `rtp.pcap`.

Si no te aparecen los paquetes RTP, prueba estas acciones:

* En el menú "Analyze" selecciona "Enabled Protocols", busca "RTP", y selecciona "rtp_udp (RTP sobre UDP)". De paso puedes seleccionar también las otras opciones de RTP sobre otros protocolos (TURN, RTSP, etc.).
* Filtra los paquetes UDP de los puertos que has usado al hacer la traza, y busca entre ellos un paquete RTP (aunque Wireshark no lo haya identificado como tal). Cuando lo encuentres, pulsa el botón derecho sobre él (para lanzar el menú de contexto) y selecciona la opción "Decode as...", eligiendo "RTP".

Más información: [RTP en Wireshark](https://wiki.wireshark.org/RTP)

## Ejercicio 3. Análisis de un flujo (stream)

Abre ahora la captura `rtp.pcap` que has realizado en el ejercicio anterior con Wireshark. 
Usando el menú Telephony | RTP puedes ver cuántos flujos (streams) RTP tenemos en la traza capturada. Para cada uno de ellos:

* ¿Cuántos paquetes hay en él?:
* ¿Qué máquina es el origen del flujo?:
* ¿Cuál es el puerto UDP de destino del flujo?:
* ¿Cuántos segundos dura el flujo?:

## Ejercicio 4. Análisis de la captura realizada

Sigue analizando los flujos RTP:

* ¿Cuál es el SSRC del flujo?:
* ¿En qué momento (en ms) de la traza comienza el flujo?
* ¿Qué número de secuencia tiene el primer paquete del flujo?
* ¿Cuál es el jitter medio del flujo?:
* ¿Cuántos paquetes del flujo se han perdido?:

## Ejercicio 5. Métricas del flujo

Llamamos "diferencia" al tiempo (en segundos) entre el momento en que llega un paquete y el momento en que "debería" haber llegado (si hubiera tardado en transmitirse exactamente lo mismo que el paquete anterior).

* ¿Cuál es la diferencia máxima en el flujo?:
* ¿Cuál es el jitter medio del flujo?:
* ¿Cuántos paquetes del flujo se han perdido?:
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:

## Ejercicio 6. Métricas de un paquete RTP

Elige un paquete cualquiera de la traza:

* ¿Cuál es su número de secuencia dentro del flujo RTP?:
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?:
* ¿Qué jitter se ha calculado para ese paquete?
* ¿Qué timestamp tiene ese paquete?
* ¿Por qué número hexadecimal empieza sus datos de audio?

## Ejercicio 7. Cambiando el codec

Usa ahora el comando siguiente, y realiza la captura igual que antes:

```commandline
gst-launch-1.0  -v filesrc location = recording.wav ! wavparse ! audioconvert ! audioresample ! gsmenc ! rtpgsmpay ! udpsink host=127.0.0.1 port=1200
```

¿Qué diferencias observas en los paquetes RTP capturados?

