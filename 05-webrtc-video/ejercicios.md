# Protocolos para la transmisión de audio y video en Internet
# Envío de video con WebRTC

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.66 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de uso de wireshark
* Nociones de UDP (de cursos anteriores)
* Nociones de WebRTC y asyncio (presentación en teoría, y prácticas anteriores)

**Tiempo estimado:** 8 horas

**Repositorio plantilla:** https://gitlab.eif.urjc.es/ptavi/2024-2025/05-webrtc-video

**Fecha de entrega parte individual:** 12 de diciembre de 2024, 23:59 (hasta ejercicio 8, incluido)

**Fecha de entrega parte interoperación:** 18 de diciembre de 2024, 23:59 (ejercicio 9)

## Introducción

WebRTC permite abrir canales para enviar datos, bien "normales", o bien multimedia. en esta práctica nos centraremos en los mecanismos de funcionamiento del módulo `aiortc` para establecer canales de video que permitan intercambiar mensajes UDP entre pares, usando WebRTC. 

## Objetivos de la práctica

* Mejorar el conocimiento de  `aiortc` y sobre todo las fases de establecimiento de la conexión entre pares, y el envío de video.
* Mejorar el conocimiento del modelo usado por `asyncio` y las funciones asíncronas (corrutinas) en Python.
* Repasar SDP, usándolo en paquetes UDP

## Ejercicio 1. Creación de repositorio para la práctica

Con el navegador, dirígete al repositorio plantilla de esta práctica y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la EIF).

## Ejercicio 2. Prueba de canales de video con aiortc

Prueba un programa que establece canales de video WebRTC entre pares, usando `aiortc`. Para ello, descarga el programa de ejemplo `cli.py` de [videostream-cli](https://github.com/aiortc/aiortc/blob/main/examples/videostream-cli/) de `aiortc`, y ejecútalo según indica su [documentación](https://github.com/aiortc/aiortc/blob/main/examples/videostream-cli/README.rst).

Por ejemplo, ejecútalo usando un video para que sea transmitido, de esta manera:

* En un terminal:

```commandline
python cli.py --record-to video-out.mp4 answer
```

* En otro terminal:

```commandline
python cli.py --play-from video.webm offer
```

Puede utilizarse el fichero [video.mebm](video.webm) (aiortc soporta audio en formato Opus, PCMU y PCMA y video en formato VP8 y H.264).

Observa cómo se establece el canal WebRTC, y luego se envía el vídeo por él (desde el ofertante hasta el que responde). Observa también cómo hay una transmisión de video en sentido inverso, y trata de ver (y entender) qué se está transmitiendo.

En este ejemplo, el mecanismo de señalización (que transfiere un documento SDP) es completamente manual: copiar el documento SDP que escribe el ofertante, pegarla cuando 
nos la pide el  que responde, y copia y pega de vuelta al ofertante para que conozca el SDP del que responde. Más adelante, mejoraremos este mecanismo de señalización, automatizándolo.

## Ejercicio 3. Cliente y servidor de video con aiortc

Prueba los ficheros `client_video_simple.py` y `server_video_simple.py`. Se comportan muy parecido al programa `cli.py` del ejercicio anterior, pero el cliente termina en cuando envía el video (que es siempre el mismo), y el servidor está diseñado para recibir un solo video, y terminar cuando recibe la señalización de despedida.

Modifícalos, creando los ficheros `client_video.py` y `server_video.py`, para que almacenen en ficheros los ficheros SDP que intercambian. El documento SDP que envía el cliente al servidor como fichero de texto `client_video.sdp`, en el formato normal de un documento SDP (una propiedad por línea). El documento SDP que envía el servidor al cliente como fichero de texto `server_video.sdp`, en el mismo formato normal de un documento SDP.

Para producir esos ficheros, puedes utilizar un `print` en cada programa, al que le pases la cadena adecuada obtenida del mensaje de señalización que se copia del cliente al servidor, o viceversa. Por ejemplo:

```python
with open("client_video.sdp", "w") as f:
    print(sdp, file=f)
```

Captura también los paquetes UDP que intercambian el cliente con el servidor, y  almacénalos (solo esos paquetes) en el fichero `video.pcap` usando wireshark.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 3".

## Ejercicio 4. Cliente-servidor con servidor de señalización UDP

Copia el fichero `client_video.py` en `client_video_udp.py` y el fichero `server_video.py` en `server_video_udp.py`, y modifícalos para que utilicen un servidor UDP para la señalización.

El servidor de señalización UDP se llamará `signalling_udp.py`. Recibirá dos tipos de mensajes de cliente y servidor:

* Un mensaje para registrarlos: será un mensaje UDP con un documento JSON, con el contenido `{"type": "REGISTER"}`, que utilizará para almacenar la dirección IP y el puerto UDP donde escuchan mensajes de señalización quien se registre, usando (el servidor de señalización) la dirección desde la que le llega el mensaje. Solo el servidor de video tendrá que registrarse. Por lo tanto, el servidor de video tendrá que estar escuchando en el puerto UDP desde el que envía el mensaje de regitsro, para recibir ahí los mensajes del servidor de señalización.

* Mensajes de SDP en formato JSON, como los que se copiaban y pegaban en el ejercicio anterior. Estos mensajes serán de tipo `offer` o `answer` (igual que en el ejercicio anterior). Si recibe un mensaje de tipo `offer`, lo trasmitirá al programa registrado (que debería ser el servidor), y si recibe un mensaje de tipo `answer` se lo enviará al cliente. La dirección del cliente la "aprenderá" cuando reciba un mensaje de tipo `offer`, dado que ese mensaje lo habrá enviado el cliente (y los clientes no se registran).

El cliente y el servidor intercambiarán los mismos datos (el video que envía el cliente) que en el apartado anterior cuando hayan intercambiado los paquetes SDP correspondientes para poder inicializar el canal de datos WebRTC.

Captura los paquetes UDP que intercambian el cliente con el servidor, y cualquiera de ellos con el servidor de señalización, y  almacénalos (solo esos paquetes) en el fichero `video_signal.pcap` usando wireshark.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 4".

## Ejercicio 5. Señalización de final

Crea nuevos ficheros `client_video_udp2.py`, `server_video_udp2.py` y `signalling_udp2.py` que funcionen como en el ejercicio anterior, pero que también reconozcan e intercambien el mensaje de señalización BYE, que llevará como contenido, en formato JSON, `{"type": "bye"}`.

Este mensaje será enviado por el cliente después de enviar el video al servidor, y antes de terminar. El mensaje será reconocido por el servidor, que terminará de funcionar en ese momento.

Captura los paquetes UDP que intercambian el cliente con el servidor, y cualquiera de ellos con el servidor de señalización, y almacénalos (solo esos paquetes) en el fichero `video_signal2.pcap` usando wireshark.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 5".

# Ejercicio 6. Varios clientes sucesivos

Crea nuevos ficheros `client_video_udp3.py`, `server_video_udp3.py` y `signalling_udp3.py` que funcionen como en el ejercicio anterior, pero de forma que el servidor funcione con varios clientes sucesivos.

En este caso, cada cliente enviará un video igual que en el ejercicio anterior, enviando ahora un mensaje de señalización BYE al acabar, que hará que el servidor alamacene el fichero de video recibido, pero no termine, sino que aceptará nuevas ofertas de clientes.

Captura los paquetes UDP que intercambian el cliente con el servidor, y cualquiera de ellos con el servidor de señalización, y almacénalos (solo esos paquetes) en el fichero `video_signal3.pcap` usando wireshark. En la captura, incluye los paquetes enviados por dos clientes sucesivos.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 6".

# Ejercicio 7. Varios clientes en paralelo

Crea nuevos ficheros `client_video_udp4.py`, `server_video_udp4.py` y `signalling_udp4.py` que funcionen como en el ejercicio anterior, pero de forma que el servidor funcione con varios clientes en paralelo. En este caso, "en paralelo" significa que antes de que termine el primer cliente, empezará el segundo, que podrá enviar el video al servidor antes de que el primer cliente termine. El servidor almacenará los videos en ficheros separados (por ejemplo, ficheros que tengan el mismo nombre, pero terminen en un número entero que se vaya incrementando cada vez que se reciba un nuevo video).

Captura los paquetes UDP que intercambian el cliente con el servidor, y cualquiera de ellos con el servidor de señalización, y  almacénalos (solo esos paquetes) en el fichero `video_signal4.pcap` usando wireshark. En la captura, incluye los paquetes enviados por dos clientes en paralelo.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 7".

# Ejercicio 8. Elección de interlocutor

En este ejercicio, crea nuevos ficheros `client_video_udp5.py`, `server_video_udp5.py` y `signalling_udp5.py` que funcionen de forma que cada servidor se identifique con un nombre único, y cada cliente indique con qué servidor se quiere comunicar. Para ello, tanto `client_video_udp5.py` como `server_video_udp5.py` admitirán un argumento (una cadena de texto), que en el caso del servidor será su nombre, y en el caso del cliente será el nombre del servidor con el que quiere comunicarse.

Al arrancar, el servidor se registrará con el servidor de señalización indicando su nombre, que el servidor de señalización almacenará, de forma que pueda redirigirle las peticiones de un cliente que demande comunicarse con él.

Al arrancar, el cliente enviará al servidor de señalización no solo su descripción de sesión, sino también (en el mismo mensaje) el nombre del servidor con el que quiere comunicar. Para hacerlo, añadirá un campo `"server": <nombre_servidor>` al documento JSON de la oferta (el que incluye la descripción de sesión).

En lo demás, los programas se comportarán como en el ejercicio anterior.

Captura los paquetes UDP que intercambian el cliente con el servidor, y cualquiera de ellos con el servidor de señalización, y  almacénalos (solo esos paquetes) en el fichero `video_signal5.pcap` usando wireshark. En la captura, incluye los paquetes enviados por al menos dos clientes que demandan dos servidores diferentes, y esos dos servidores.

Realiza un commit al terminar este ejercicio, cuya primera línea de comentario sea "Ejercicio 8".


## Ejercicio 9 (segundo periodo)

Para realizar esta práctica, tendrás que comenzar creando (si no lo has hecho antes) un nuevo grupo en el GitLab de la EIF, como se describe en el ejercico 11 de la práctica [01-entorno](../01-entorno/ejercicios.md).

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del repositorio plantilla de esta práctica uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la EIF de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `05-webrtc-video-<alumno>`.

Crea un clon local de este nuevo repositorio, y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Llama a esta rama `nueva` (ver detalles en la práctica 01-entorno). 

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Crea en ella un nuevo programa, llamado `server.py` que actúe como un servidor que se llame `hola`, utilizando el código que `alumno` ha desarrollado para su servidor, pero que en lugar de pedir el nombre al arrancar, simplemente asuma que se llama `hola` . Prueba que funciona (como indica el enunciado del Ejercicio 8).

Cuando termines, crea un commit que incluya este nuevo fichero, y que tenga un comentario cuya primera línea sea "Ejercicio 9". Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes usar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).

## ¿Qué se valora de esta la práctica?

En está práctica valoraremos sobre todo que el código desarrollado realice lo indicado con la mayor exactitud posible. Además, la legibilidad del código, y en general el seguimiento estricto de las instrucciones serán tenidos en cuenta.


## Créditos

* [video.mebm](video.webm) es un fragmento de video de [Big Buck Bunny](https://peach.blender.org/).