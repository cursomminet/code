## WebRTC video

Actividades:

* Uso de WebRTC para transmitir video entre pares
* Descripción del ejercicio de entrega

## Ejercicios

[Ejercicios de entrega evaluable (minipráctica)](ejercicios.md)

Referencias:

* [aiortc](https://github.com/aiortc/aiortc)
* [WebRTC](https://webrtc.org/)
* [Get started with WebRTC](https://web.dev/articles/webrtc-basics)
* [WebRTC API](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
